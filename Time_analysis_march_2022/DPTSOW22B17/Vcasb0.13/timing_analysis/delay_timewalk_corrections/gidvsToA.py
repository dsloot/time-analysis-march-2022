#!/usr/bin/env python3

import os, yaml
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from math import ceil


def read_yaml(filename):
    with open(filename, 'r') as file:
        config = yaml.safe_load(file)
    return config


if __name__ == "__main__":
	dpts_dict = read_yaml(os.getcwd() + r"/DPTS.yml")

	# load dataset
	df = pd.read_csv(os.getcwd() + r'/../analysis/dataframe_csv_files' + r'/filtered_dataframe.csv', sep='|', header=0)
	
	# DPTS_0
	dpts_0 = df[df['detector'] == 'DPTS_0']
	dpts_0 = dpts_0.query('timestamp < 3001 & timestamp > 499')
	# values
	dpts0ToA = (dpts_0['timestamp'].values.astype('float'))  # in ns
	dpts0gid = (dpts_0['px_cluster_column'].values.astype('float'))  # in ns

	dpts0ToA_even_even = np.array([])
	dpts0gid_even_even= np.array([])
	dpts0ToA_even_odd= np.array([])
	dpts0gid_even_odd= np.array([])
	dpts0ToA_odd_even= np.array([])
	dpts0gid_odd_even= np.array([])
	dpts0ToA_odd_odd= np.array([])
	dpts0gid_odd_odd= np.array([])

	for i in range(len(df['px_cluster_column'])):
		ToA = df.at[i, 'timestamp'].astype('float')
		GID = df.at[i, 'px_cluster_column'].astype('float')
		if df.at[i, 'detector'] == 'DPTS_0' and df.at[i, 'px_cluster_column'] % 2 == 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 == 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				dpts0ToA_even_even = np.append(dpts0ToA_even_even, ToA)  # in ns
				dpts0gid_even_even = np.append(dpts0gid_even_even, GID)  # in ns

			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 != 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				dpts0ToA_even_odd = np.append(dpts0ToA_even_odd, ToA)  # in ns
				dpts0gid_even_odd = np.append(dpts0gid_even_odd, GID)  # in ns

		if df.at[i, 'detector'] == 'DPTS_0' and df.at[i, 'px_cluster_column'] % 2 != 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 == 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				dpts0ToA_odd_even = np.append(dpts0ToA_odd_even, ToA)  # in ns
				dpts0gid_odd_even = np.append(dpts0gid_odd_even, GID)  # in ns

			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 != 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				dpts0ToA_odd_odd = np.append(dpts0ToA_odd_odd, ToA)  # in ns
				dpts0gid_odd_odd = np.append(dpts0gid_odd_odd, GID)  # in ns

	# create plot directory if it doesn't exist
	plot_dir = os.getcwd() + r'/gid_vs_ToA_correlation_plots'
	if not os.path.isdir(plot_dir):
		os.makedirs(plot_dir)

	# GID plot
	# Creating bins
	bin_width_gid = 0.10  # [ns]
	bin_width_ToA = 3      # [ns]
	x_min = np.min(dpts0gid)
	x_max = np.max(dpts0gid)
	y_min = np.min(dpts0ToA)
	y_max = np.max(dpts0ToA)
	x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_gid))
	y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA))
	## Compute 2d histogram
	H_dpts0_gid, ToAedges_dpts0, GIDedges_dpts0 = np.histogram2d(dpts0ToA, dpts0gid, bins=[y_bins, x_bins])
	# Find the bin centres
	GIDbinsctr_dpts0 = np.array([0.5 * (GIDedges_dpts0[i] + GIDedges_dpts0[i+1]) for i in range(len(GIDedges_dpts0)-1)])
	ToAbinsctr_dpts0 = np.array([0.5 * (ToAedges_dpts0[i] + ToAedges_dpts0[i+1]) for i in range(len(ToAedges_dpts0)-1)])
	fig1, ax1 = plt.subplots(figsize =(21, 10)) 
	im1 = ax1.pcolormesh(GIDedges_dpts0, ToAedges_dpts0, H_dpts0_gid) # , cmap='rainbow')
	ax1.set_xlim(dpts0gid.min(), dpts0gid.max())
	# ax1.plot(GIDbinsctr_dpts0, ToAbinavg_dpts0, 'ko')  # peak value of each GID bin
	# ax1.plot(plot_x, plot_y, 'r-')
	# ax1.set_ylim(750, 1500)	
	ax1.set_ylim(dpts0ToA.min(), dpts0ToA.max())
	ax1.set_xlabel('Column number', fontsize=25)  # , fontweight='bold')
	ax1.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
	ax1.set_title('ToA/Column correlation - {} - vcasb0.13'.format(dpts_dict['DPTS_0']), fontsize=25)
	cb1 = plt.colorbar(im1, ax=ax1)
	cb1.ax.tick_params(labelsize=25)
	plt.xticks(fontsize=25)
	plt.yticks(fontsize=25)
	ax1.grid()
	plt.tight_layout()
	# plt.show()
	plt.close()
	fig1.savefig(plot_dir + r'/all_gid_vs_ToA_correlation_plots.jpg', dpi=fig1.dpi, bbox_inches='tight', pad_inches=0.5)

	# GID plot even_even
	# Creating bins
	bin_width_gid = 0.10  # [ns]
	bin_width_ToA = 3      # [ns]
	x_min = np.min(dpts0gid_even_even)
	x_max = np.max(dpts0gid_even_even)
	y_min = np.min(dpts0ToA_even_even)
	y_max = np.max(dpts0ToA_even_even)
	x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_gid))
	y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA))
	## Compute 2d histogram
	H_dpts0_gid_even_even, ToAedges_dpts0_even_even, GIDedges_dpts0_even_even = np.histogram2d(dpts0ToA_even_even, dpts0gid_even_even, bins=[y_bins, x_bins])
	# Find the bin centres
	GIDbinsctr_dpts0_even_even = np.array([0.5 * (GIDedges_dpts0_even_even[i] + GIDedges_dpts0_even_even[i+1]) for i in range(len(GIDedges_dpts0_even_even)-1)])
	ToAbinsctr_dpts0_even_even = np.array([0.5 * (ToAedges_dpts0_even_even[i] + ToAedges_dpts0_even_even[i+1]) for i in range(len(ToAedges_dpts0_even_even)-1)])
	fig2, ax2 = plt.subplots(figsize =(21, 10)) 
	im2 = ax2.pcolormesh(GIDedges_dpts0_even_even, ToAedges_dpts0_even_even, H_dpts0_gid_even_even) # , cmap='rainbow')
	# ax2.set_xlim(dpts0gid_even_even.min(), dpts0gid_even_even.max())
	ax2.set_xlim(dpts0gid.min(), dpts0gid.max())
	# ax2.plot(GIDbinsctr_dpts0_even_even, ToAbinavg_dpts0_even_even, 'ko')  # peak value of each GID bin
	# ax2.plot(plot_x, plot_y, 'r-')
	# ax2.set_xlim(dpts0gid_even_even.min(), 1150)
	#ax2.set_ylim(750, 1500)	
	ax2.set_ylim(dpts0ToA.min(), dpts0ToA.max())
	ax2.set_xlabel('Column number', fontsize=25)  # , fontweight='bold')
	ax2.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
	ax2.set_title('ToA/Column correlation - {} - vcasb0.13: even-even'.format(dpts_dict['DPTS_0']), fontsize=25)
	cb1 = plt.colorbar(im2, ax=ax2)
	cb1.ax.tick_params(labelsize=25)
	plt.xticks(fontsize=25)
	plt.yticks(fontsize=25)
	ax2.grid()
	plt.tight_layout()
	# plt.show()
	plt.close()
	fig2.savefig(plot_dir + r'/even_even_gid_vs_ToA_correlation_plots.jpg', dpi=fig2.dpi, bbox_inches='tight', pad_inches=0.5)

	# GID plot even_odd
	# Creating bins
	bin_width_gid = 0.10  # [ns]
	bin_width_ToA = 3      # [ns]
	x_min = np.min(dpts0gid_even_odd)
	x_max = np.max(dpts0gid_even_odd)
	y_min = np.min(dpts0ToA_even_odd)
	y_max = np.max(dpts0ToA_even_odd)
	x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_gid))
	y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA))
	## Compute 2d histogram
	H_dpts0_gid_even_odd, ToAedges_dpts0_even_odd, GIDedges_dpts0_even_odd = np.histogram2d(dpts0ToA_even_odd, dpts0gid_even_odd, bins=[y_bins, x_bins])
	# Find the bin centres
	GIDbinsctr_dpts0_even_odd = np.array([0.5 * (GIDedges_dpts0_even_odd[i] + GIDedges_dpts0_even_odd[i+1]) for i in range(len(GIDedges_dpts0_even_odd)-1)])
	ToAbinsctr_dpts0_even_odd = np.array([0.5 * (ToAedges_dpts0_even_odd[i] + ToAedges_dpts0_even_odd[i+1]) for i in range(len(ToAedges_dpts0_even_odd)-1)])
	fig3, ax3 = plt.subplots(figsize =(21, 10)) 
	im3 = ax3.pcolormesh(GIDedges_dpts0_even_odd, ToAedges_dpts0_even_odd, H_dpts0_gid_even_odd) # , cmap='rainbow')
	# ax3.set_xlim(dpts0gid_even_odd.min(), dpts0gid_even_odd.max())
	ax3.set_xlim(dpts0gid.min(), dpts0gid.max())
	# ax3.plot(GIDbinsctr_dpts0_even_odd, ToAbinavg_dpts0_even_odd, 'ko')  # peak value of each GID bin
	# ax3.plot(plot_x, plot_y, 'r-')
	# ax3.set_xlim(dpts0gid_even_odd.min(), 1150)
	#ax3.set_ylim(750, 1500)	
	ax3.set_ylim(dpts0ToA.min(), dpts0ToA.max())
	ax3.set_xlabel('Column number', fontsize=25)  # , fontweight='bold')
	ax3.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
	ax3.set_title('ToA/Column correlation - {} - vcasb0.13: even-odd'.format(dpts_dict['DPTS_0']), fontsize=25)
	cb1 = plt.colorbar(im3, ax=ax3)
	cb1.ax.tick_params(labelsize=25)
	plt.xticks(fontsize=25)
	plt.yticks(fontsize=25)
	ax3.grid()
	plt.tight_layout()
	# plt.show()
	plt.close()
	fig3.savefig(plot_dir + r'/even_odd_gid_vs_ToA_correlation_plots.jpg', dpi=fig3.dpi, bbox_inches='tight', pad_inches=0.5)

	# GID plot odd_even
	# Creating bins
	bin_width_gid = 0.10  # [ns]
	bin_width_ToA = 3      # [ns]
	x_min = np.min(dpts0gid_odd_even)
	x_max = np.max(dpts0gid_odd_even)
	y_min = np.min(dpts0ToA_odd_even)
	y_max = np.max(dpts0ToA_odd_even)
	x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_gid))
	y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA))
	## Compute 2d histogram
	H_dpts0_gid_odd_even, ToAedges_dpts0_odd_even, GIDedges_dpts0_odd_even = np.histogram2d(dpts0ToA_odd_even, dpts0gid_odd_even, bins=[y_bins, x_bins])
	# Find the bin centres
	GIDbinsctr_dpts0_odd_even = np.array([0.5 * (GIDedges_dpts0_odd_even[i] + GIDedges_dpts0_odd_even[i+1]) for i in range(len(GIDedges_dpts0_odd_even)-1)])
	ToAbinsctr_dpts0_odd_even = np.array([0.5 * (ToAedges_dpts0_odd_even[i] + ToAedges_dpts0_odd_even[i+1]) for i in range(len(ToAedges_dpts0_odd_even)-1)])
	fig4, ax4 = plt.subplots(figsize =(21, 10)) 
	im4 = ax4.pcolormesh(GIDedges_dpts0_odd_even, ToAedges_dpts0_odd_even, H_dpts0_gid_odd_even) # , cmap='rainbow')
	# ax4.set_xlim(dpts0gid_odd_even.min(), dpts0gid_odd_even.max())
	ax4.set_xlim(dpts0gid.min(), dpts0gid.max())
	# ax4.plot(GIDbinsctr_dpts0_odd_even, ToAbinavg_dpts0_odd_even, 'ko')  # peak value of each GID bin
	# ax4.plot(plot_x, plot_y, 'r-')
	# ax4.set_xlim(dpts0gid_odd_even.min(), 1150)
	#ax4.set_ylim(750, 1500)	
	ax4.set_ylim(dpts0ToA.min(), dpts0ToA.max())
	ax4.set_xlabel('Column number', fontsize=25)  # , fontweight='bold')
	ax4.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
	ax4.set_title('ToA/Column correlation - {} - vcasb0.13: odd_even'.format(dpts_dict['DPTS_0']), fontsize=25)
	cb1 = plt.colorbar(im4, ax=ax4)
	cb1.ax.tick_params(labelsize=25)
	plt.xticks(fontsize=25)
	plt.yticks(fontsize=25)
	ax4.grid()
	plt.tight_layout()
	# plt.show()
	plt.close()
	fig4.savefig(plot_dir + r'/odd_even_gid_vs_ToA_correlation_plots.jpg', dpi=fig4.dpi, bbox_inches='tight', pad_inches=0.5)

	# GID plot odd_odd
	# Creating bins
	bin_width_gid = 0.10  # [ns]
	bin_width_ToA = 3      # [ns]
	x_min = np.min(dpts0gid_odd_odd)
	x_max = np.max(dpts0gid_odd_odd)
	y_min = np.min(dpts0ToA_odd_odd)
	y_max = np.max(dpts0ToA_odd_odd)
	x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_gid))
	y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA))
	## Compute 2d histogram
	H_dpts0_gid_odd_odd, ToAedges_dpts0_odd_odd, GIDedges_dpts0_odd_odd = np.histogram2d(dpts0ToA_odd_odd, dpts0gid_odd_odd, bins=[y_bins, x_bins])
	# Find the bin centres
	GIDbinsctr_dpts0_odd_odd = np.array([0.5 * (GIDedges_dpts0_odd_odd[i] + GIDedges_dpts0_odd_odd[i+1]) for i in range(len(GIDedges_dpts0_odd_odd)-1)])
	ToAbinsctr_dpts0_odd_odd = np.array([0.5 * (ToAedges_dpts0_odd_odd[i] + ToAedges_dpts0_odd_odd[i+1]) for i in range(len(ToAedges_dpts0_odd_odd)-1)])
	fig5, ax5 = plt.subplots(figsize =(21, 10)) 
	im5 = ax5.pcolormesh(GIDedges_dpts0_odd_odd, ToAedges_dpts0_odd_odd, H_dpts0_gid_odd_odd) # , cmap='rainbow')
	# ax5.set_xlim(dpts0gid_odd_odd.min(), dpts0gid_odd_odd.max())
	ax5.set_xlim(dpts0gid.min(), dpts0gid.max())
	# ax5.plot(GIDbinsctr_dpts0_odd_odd, ToAbinavg_dpts0_odd_odd, 'ko')  # peak value of each GID bin
	# ax5.plot(plot_x, plot_y, 'r-')
	# ax5.set_xlim(dpts0gid_odd_odd.min(), 1150)
	#ax5.set_ylim(750, 1500)	
	ax5.set_ylim(dpts0ToA.min(), dpts0ToA.max())
	ax5.set_xlabel('Column number', fontsize=25)  # , fontweight='bold')
	ax5.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
	ax5.set_title('ToA/Column correlation - {} - vcasb0.13: odd-odd'.format(dpts_dict['DPTS_0']), fontsize=25)
	cb1 = plt.colorbar(im5, ax=ax5)
	cb1.ax.tick_params(labelsize=25)
	plt.xticks(fontsize=25)
	plt.yticks(fontsize=25)
	ax5.grid()
	plt.tight_layout()
	# plt.show()
	plt.close()
	fig5.savefig(plot_dir + r'/odd_odd_gid_vs_ToA_correlation_plots.jpg', dpi=fig5.dpi, bbox_inches='tight', pad_inches=0.5)
