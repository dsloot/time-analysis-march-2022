# DPTS Testbeam Analysis - timing analysis

Python software to analyze Corryvreckan output files and conclude the final timing resolution analysis

_______________
_______________

## Usage principles

All the scripts have been designed to be used as terminal commands. Use the -h flag to access the help.
The scripts read the configuration files DPTS.yml to complete the analysis and produce the plots.

1) Modify DPTS.yml with the names of the devices to be analyzed - Example provided for DESY March 2022 DUT, Vbb = -1.2 V
2) Run timing_analysis.py to filter the output .csv file and safe it for further analysis.
3) Run plot_correlations.py to plot the ToA and ToT correlations from the data.

_________________________
_________________________

## Example

List of commands with example flags. (Before corrections applied)

	$ ./timing_analysis.py -f ../../ouput/outfile.csv -s -g
	$ ./plot_correlations.py -f dataframe_csv_files/filtered_dataframe.csv -p -tot

List of commands with example flags. (After corrections applied)
	
	$ ./timing_analysis.py -f ../dataframe_csv_files/updated_dataframe.csv -u -w -s -g
	$ ./plot_correlations.py -f dataframe_csv_files/complete_dataframe.csv -o -t -g