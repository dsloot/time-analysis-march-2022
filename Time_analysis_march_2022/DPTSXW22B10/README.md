# General templates for beam test data analysis 

Configuration files for telescope software alignment and DUT analysis. Refer to Corryvreckan manual for more details.

- 00_read_data.conf: data visualisation
- 01_prealign_telescope.conf: telescope prealignment (tracking planes + DUTs)
- 02_telescope_alignment.conf: telescope alignment (tracking planes + DUTs)
- 03_residuals_check.conf: creates plots of tracks and DUT residuals to check the quality of the alignment
- 03_z_read_data.conf: further iteration for DUT alignment. To be used only if the DUT residuals produced by the previous module are not satisfactory
- 04_dut_analysis.conf: DUT spatial and time (if present) residuals + chip efficiency analysis