#!/bin/bash

for i in `cat filelist_March2022.txt`
do
	sed 's+file_name = ""+file_name = "/home/pixel/TimeResolution_Analysis/DPTSOW22B17_analysis/vcasb0.25/data/'${i}'.raw"+g' /home/pixel/TimeResolution_Analysis/DPTSOW22B17_analysis/vcasb0.25/dut_analysis.conf > /home/pixel/TimeResolution_Analysis/DPTSOW22B17_analysis/vcasb0.25/analysis_config_files/dut_analysis-${i}.conf	
	/home/pixel/installations/corryvreckan/bin/corry -c /home/pixel/TimeResolution_Analysis/DPTSOW22B17_analysis/vcasb0.25/analysis_config_files/dut_analysis-${i}.conf -o histogram_file=${i}.root
done
