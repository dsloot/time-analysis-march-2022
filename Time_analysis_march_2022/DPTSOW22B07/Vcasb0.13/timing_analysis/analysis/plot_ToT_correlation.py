#!/usr/bin/env python3

import os, argparse, yaml
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.stats import norm
from math import ceil

def arguments():
    parser=argparse.ArgumentParser(description='Plot ToT/ToA correlation')
    parser.add_argument('--ToT_plot',			'-tot',	 action='store_true',      help='plot ToT histogram')
    parser.add_argument('--ToT_bin_width',	    '-tbw',  type=float, default=500,   help='histogram ToT bin width in ns - default: 90')
    parser.add_argument('--ToA_bin_width',	    '-abw',  type=float, default=5,    help='histogram ToA bin width in ns - default: 2')    
    args=parser.parse_args()
    return args


def read_yaml(filename):
    with open(filename, 'r') as file:
        config = yaml.safe_load(file)
    return config


if __name__ == "__main__":
	args = arguments()
	dpts_dict = read_yaml(os.getcwd() + r"/DPTS.yml")	
	# load dataset from given directory
	df1 = pd.read_csv(os.getcwd() + r'/dataframe_csv_files' + r'/filtered_dataframe.csv', sep='|', header=0)
	df2 = pd.read_csv(os.getcwd() + r'/../../../vcasb0.25/timing_analysis/analysis/dataframe_csv_files' + r'/filtered_dataframe.csv', sep='|', header=0)
	df3 = pd.read_csv(os.getcwd() + r'/../../../../DPTSOW22B17_analysis/vcasb0.13/timing_analysis/analysis/dataframe_csv_files' + r'/filtered_dataframe.csv', sep='|', header=0)
	df4 = pd.read_csv(os.getcwd() + r'/../../../../DPTSOW22B17_analysis/vcasb0.25/timing_analysis/analysis/dataframe_csv_files' + r'/filtered_dataframe.csv', sep='|', header=0)
	df5 = pd.read_csv(os.getcwd() + r'/../../../../DPTSOW22B7_analysis_Ireset10/vcasb0.13/timing_analysis/analysis/dataframe_csv_files' + r'/filtered_dataframe.csv', sep='|', header=0)
	df6 = pd.read_csv(os.getcwd() + r'/../../../../DPTSOW22B7_analysis_Ireset10/vcasb0.25/timing_analysis/analysis/dataframe_csv_files' + r'/filtered_dataframe.csv', sep='|', header=0)
	df7 = pd.read_csv(os.getcwd() + r'/dataframe_csv_files' + r'/ToT_ToA_dataframe.csv', sep='|', header=0)

	plot_dir0 = os.getcwd() + r'/ToT_plots'
	for d in [plot_dir0]:
		if not os.path.isdir(d):
			os.makedirs(d)

	# Creating bins
	bin_width_ToT = args.ToT_bin_width  # ns 
	bin_width_ToA = args.ToA_bin_width  # ns
	
	dpts1_0 = df1[df1['detector'] == 'DPTS_0']
	dpts1_0 = dpts1_0.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts1_1 = df1[df1['detector'] == 'DPTS_1']	
	dpts1_1 = dpts1_1.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts2_0 = df2[df2['detector'] == 'DPTS_0']
	dpts2_0 = dpts2_0.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts2_1= df2[df2['detector'] == 'DPTS_1']	
	dpts2_1 = dpts2_1.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts3_0 = df3[df3['detector'] == 'DPTS_0']
	dpts3_0 = dpts3_0.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts3_1= df3[df3['detector'] == 'DPTS_1']	
	dpts3_1 = dpts3_1.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts4_0 = df4[df4['detector'] == 'DPTS_0']
	dpts4_0 = dpts4_0.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts4_1= df4[df4['detector'] == 'DPTS_1']	
	dpts4_1 = dpts4_1.query('timestamp < 3001 & timestamp > 499')  # SQL way	
	dpts5_0 = df5[df5['detector'] == 'DPTS_0']
	dpts5_0 = dpts5_0.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts5_1= df5[df5['detector'] == 'DPTS_1']	
	dpts5_1 = dpts5_1.query('timestamp < 3001 & timestamp > 499')  # SQL way	
	dpts6_0 = df6[df6['detector'] == 'DPTS_0']
	dpts6_0 = dpts6_0.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts6_1= df6[df6['detector'] == 'DPTS_1']	
	dpts6_1 = dpts6_1.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts7_0 = df7[df7['detector'] == 'DPTS_3']
	dpts7_0 = dpts7_0.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts7_1= df7[df7['detector'] == 'DPTS_4']	
	dpts7_1 = dpts7_1.query('timestamp < 3001 & timestamp > 499')  # SQL way		

	if args.ToT_plot:

		dpts0ToT1 = (dpts1_0['ToT'].values.astype('float'))  # in ns
		x0_min1 = np.min(dpts0ToT1)
		x0_max1 = np.max(dpts0ToT1)
		x0_bins1 = np.linspace(x0_min1, x0_max1, ceil((x0_max1-x0_min1)/bin_width_ToT))
		dpts1ToT1 = (dpts1_1['ToT'].values.astype('float'))  # in ns
		x1_min1 = np.min(dpts1ToT1)
		x1_max1 = np.max(dpts1ToT1)
		x1_bins1 = np.linspace(x1_min1, x1_max1, ceil((x1_max1-x1_min1)/bin_width_ToT))

		dpts0ToT2 = (dpts2_0['ToT'].values.astype('float'))  # in ns
		x0_min2 = np.min(dpts0ToT2)
		x0_max2 = np.max(dpts0ToT2)
		x0_bins2 = np.linspace(x0_min2, x0_max2, ceil((x0_max2-x0_min2)/bin_width_ToT))
		dpts1ToT2 = (dpts2_1['ToT'].values.astype('float'))  # in ns
		x1_min2 = np.min(dpts1ToT2)
		x1_max2 = np.max(dpts1ToT2)
		x1_bins2 = np.linspace(x1_min2, x1_max2, ceil((x1_max2-x1_min2)/bin_width_ToT))

		dpts0ToT3 = (dpts3_0['ToT'].values.astype('float'))  # in ns
		x0_min3 = np.min(dpts0ToT3)
		x0_max3 = np.max(dpts0ToT3)
		x0_bins3 = np.linspace(x0_min3, x0_max3, ceil((x0_max3-x0_min3)/bin_width_ToT))
		dpts1ToT3 = (dpts3_1['ToT'].values.astype('float'))  # in ns
		x1_min3 = np.min(dpts1ToT3)
		x1_max3 = np.max(dpts1ToT3)
		x1_bins3 = np.linspace(x1_min3, x1_max3, ceil((x1_max3-x1_min3)/bin_width_ToT))

		dpts0ToT4 = (dpts4_0['ToT'].values.astype('float'))  # in ns
		x0_min4 = np.min(dpts0ToT4)
		x0_max4 = np.max(dpts0ToT4)
		x0_bins4 = np.linspace(x0_min4, x0_max4, ceil((x0_max4-x0_min4)/bin_width_ToT))
		dpts1ToT4 = (dpts4_1['ToT'].values.astype('float'))  # in ns
		x1_min4 = np.min(dpts1ToT4)
		x1_max4 = np.max(dpts1ToT4)
		x1_bins4 = np.linspace(x1_min4, x1_max4, ceil((x1_max4-x1_min4)/bin_width_ToT))

		dpts0ToT5 = (dpts5_0['ToT'].values.astype('float'))  # in ns
		x0_min5 = np.min(dpts0ToT5)
		x0_max5 = np.max(dpts0ToT5)
		x0_bins5 = np.linspace(x0_min5, x0_max5, ceil((x0_max5-x0_min5)/bin_width_ToT))
		dpts1ToT5 = (dpts5_1['ToT'].values.astype('float'))  # in ns
		x1_min5 = np.min(dpts1ToT5)
		x1_max5 = np.max(dpts1ToT5)
		x1_bins5 = np.linspace(x1_min5, x1_max5, ceil((x1_max5-x1_min5)/bin_width_ToT))

		dpts0ToT6 = (dpts6_0['ToT'].values.astype('float'))  # in ns
		x0_min6 = np.min(dpts0ToT6)
		x0_max6 = np.max(dpts0ToT6)
		x0_bins6 = np.linspace(x0_min6, x0_max6, ceil((x0_max6-x0_min6)/bin_width_ToT))
		dpts1ToT6 = (dpts6_1['ToT'].values.astype('float'))  # in ns
		x1_min6 = np.min(dpts1ToT6)
		x1_max6 = np.max(dpts1ToT6)
		x1_bins6 = np.linspace(x1_min6, x1_max6, ceil((x1_max6-x1_min6)/bin_width_ToT))

		dpts0ToT7 = (dpts7_0['ToT'].values.astype('float'))  # in ns
		x0_min7 = np.min(dpts0ToT7)
		x0_max7 = np.max(dpts0ToT7)
		x0_bins7 = np.linspace(x0_min7, x0_max7, ceil((x0_max7-x0_min7)/bin_width_ToT))
		dpts1ToT7 = (dpts7_1['ToT'].values.astype('float'))  # in ns
		x1_min7 = np.min(dpts1ToT7)
		x1_max7 = np.max(dpts1ToT7)
		x1_bins7 = np.linspace(x1_min7, x1_max7, ceil((x1_max7-x1_min7)/bin_width_ToT))
		
		fig1, ax1 = plt.subplots(figsize =(15, 10))
		#im1 = ax1.hist(dpts1ToT2, bins = x1_bins2, color='#377eb8', density=True, alpha=0.7, label='B06')
		#im2 = ax1.hist(dpts0ToT2, bins = x0_bins2, color='#ff7f00', density=True, alpha=0.7, label='B07')
		im1 = ax1.hist(dpts0ToT1, bins = x0_bins1, histtype='step', color='#ff7f00', density=True, label='B07 (Vcasb0.13)')
		im2 = ax1.hist(dpts0ToT2, bins = x0_bins2, histtype='step', color='magenta', density=True, label='B07 (Vcasb0.25)')
		im3 = ax1.hist(dpts0ToT3, bins = x0_bins3, histtype='step', color='#4daf4a', density=True, label='B17 (Vcasb0.13)')
		im4 = ax1.hist(dpts0ToT4, bins = x0_bins4, histtype='step', color='#377eb8', density=True, label='B17 (Vcasb0.25)')
		#im1 = ax1.hist(dpts1ToT7, bins = x1_bins7, histtype='step', color='#ff7f00', density=True, label='B01')
		#im2 = ax1.hist(dpts0ToT7, bins = x0_bins7, histtype='step', color='magenta', density=True, label='B03')
		#im3 = ax1.hist(dpts1ToT2, bins = x1_bins2, histtype='step', color='#377eb8', density=True, label='B06')
		#im4 = ax1.hist(dpts0ToT2, bins = x0_bins2, histtype='step', color='#377eb8', density=True, label='B07')
		ax1.set_xlim(0, 40000)
		ax1.set_ylim(0, 0.0002)
		ax1.set_xlabel('ToT (ns)', fontsize=24, fontweight='bold')  # , fontweight='bold')
		ax1.set_ylabel('Fraction of events (per {} ns)'.format(bin_width_ToT), fontsize=24, fontweight='bold') 
		ax1.set_title('Normalized ToT distribution', fontsize=24)
		plt.xticks(fontsize=22)
		plt.yticks(fontsize=22)
		plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
		ax1.yaxis.get_offset_text().set_fontsize(15)
		ax1.grid()
		plt.tight_layout()
		plt.legend(loc='upper right',  prop={'size': 18})

		x = 0.64
		y = 0.98
		ax1.text(
			x-0.17,y,
			# '$\\bf{ITS3}$ beam test $\\it{%s}$',
			'Test beam preliminary $\\it{%s}$',
			fontsize=25,
			ha='center', va='top',
			# color='white',
			transform=ax1.transAxes
		)		
		#ax1.text(
		#	# x-0.01,y-0.06,
		#	x-0.17,y-0.06,
		#	'B01 & B03: @DESY Sept 2021, 5.4 GeV/c electrons',
		#	fontsize=22,
		#	ha='center', va='top',
		#	# color='white',
		#	transform=ax1.transAxes
		#)
		ax1.text(
			# x-0.01,y-0.06,
			x-0.17,y-0.06,
			'@DESY March 2022, 3.4 GeV/c electrons',
			fontsize=22,
			ha='center', va='top',
			# color='white',
			transform=ax1.transAxes
		)
		ax1.text(
			x-0.17,y-0.12,
			'Plotted on 5 July 2022',   
			fontsize=18,
			ha='center', va='top',
			# color='white',
			transform=ax1.transAxes
		)
		#ax1.text(1.02,1,
		#	'\n'.join([
		#		'$\\bf{%s}$'%'DPTSOW22B06 \ (not \ irradiated)',
		#		# 'wafer: %s'%'22',
		#		# 'chip: %s'%'3',
		#		'version: %s'%'O',
		#		'split:  %s'%'4 (opt.)',
		#		'$I_{reset}=%s\,\\mathrm{pA}$' %10,
		#		'$I_{bias}=%s\,\\mathrm{nA}$' %100,
		#		'$I_{biasn}=%s\,\\mathrm{nA}$' %10,
		#		'$I_{db}=%s\,\\mathrm{nA}$'   %50,
		#		'$V_{casn}=%s\,\\mathrm{mV}$' %300,
		#		'$V_{casb}=%s\,\\mathrm{mV}$' %200,
		#		'$V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
		#		'$thr\\approx%s\\mathrm{e^{-}}$' %200
		#	]),
		#	fontsize=15,
		#	ha='left', va='top',
		#	transform=ax1.transAxes
		#)	
		ax1.text(1.02,1,
			'\n'.join([
				'$\\bf{%s}$'%'DPTSOW22B07 \ (not \ irradiated)',
				# 'wafer: %s'%'22',
				# 'chip: %s'%'3',
				'version: %s'%'O',
				'split:  %s'%'4 (opt.)',
				'$I_{reset}=%s\,\\mathrm{pA}$' %35,
				'$I_{bias}=%s\,\\mathrm{nA}$' %100,
				'$I_{biasn}=%s\,\\mathrm{nA}$' %10,
				'$I_{db}=%s\,\\mathrm{nA}$'   %50,
				'$V_{casn}=%s\,\\mathrm{mV}$' %300,
				'$V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
				'$V_{casb}=%s\,&\,%s\,\\mathrm{mV}$' %(130, 250),
				'$thr=%s\,&\,%s\\mathrm{e^{-}}$' %(326, 156)
			]),
			fontsize=15,
			ha='left', va='top',
			transform=ax1.transAxes
		)	
		ax1.text(1.02,0.6,
			'\n'.join([
				'$\\bf{%s}$'%'DPTSOW22B17 \ (irradiated)',
				# 'wafer: %s'%'22',
				# 'chip: %s'%'3',
				'$10^{15}$ 1MeV n$_{eq}$ cm$^2$',
				'version: %s'%'O',
				'split:  %s'%'4 (opt.)',
				'$I_{reset}=%s\,\\mathrm{pA}$' %35,
				'$I_{bias}=%s\,\\mathrm{nA}$' %100,
				'$I_{biasn}=%s\,\\mathrm{nA}$' %10,
				'$I_{db}=%s\,\\mathrm{nA}$'   %50,
				'$V_{casn}=%s\,\\mathrm{mV}$' %300,
				'$V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
				'$V_{casb}=%s\,&\,%s\,\\mathrm{mV}$' %(130, 250),
				'$thr=%s\,&\,%s\\mathrm{e^{-}}$' %(326, 156)
			]),
			fontsize=15,
			ha='left', va='top',
			transform=ax1.transAxes
		)	
		#ax1.text(1.02,1.05,
		#	'\n'.join([
		#		'$\\bf{%s}$'%'DPTSXW22B01 \ (not \ irradiated)',
		#		# 'wafer: %s'%'22',
		#		# 'chip: %s'%'3',
		#		'version: %s'%'X',
		#		'split:  %s'%'4 (opt.)',
		#		'$I_{reset}=%s\,\\mathrm{pA}$' %10,
		#		'$I_{bias}=%s\,\\mathrm{nA}$' %100,
		#		'$I_{biasn}=%s\,\\mathrm{nA}$' %10,
		#		'$I_{db}=%s\,\\mathrm{nA}$'   %100,
		#		'$V_{casn}=%s\,\\mathrm{mV}$' %300,
		#		'$V_{casb}=%s\,\\mathrm{mV}$' %280,
		#		'$V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
		#		'$thr=%s\\mathrm{e^{-}}$' %110
		#	]),
		#	fontsize=15,
		#	ha='left', va='top',
		#	transform=ax1.transAxes
		#)	
		#ax1.text(1.02,0.7,
		#	'\n'.join([
		#		'$\\bf{%s}$'%'DPTSOW22B03 \ (not \ irradiated)',
		#		# 'wafer: %s'%'22',
		#		# 'chip: %s'%'3',
		#		'version: %s'%'O',
		#		'split:  %s'%'4 (opt.)',
		#		'$I_{reset}=%s\,\\mathrm{pA}$' %10,
		#		'$I_{bias}=%s\,\\mathrm{nA}$' %100,
		#		'$I_{biasn}=%s\,\\mathrm{nA}$' %10,
		#		'$I_{db}=%s\,\\mathrm{nA}$'   %100,
		#		'$V_{casn}=%s\,\\mathrm{mV}$' %300,
		#		'$V_{casb}=%s\,\\mathrm{mV}$' %250,
		#		'$V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
		#		'$thr=%s\\mathrm{e^{-}}$' %140
		#	]),
		#	fontsize=15,
		#	ha='left', va='top',
		#	transform=ax1.transAxes
		#)		
		#ax1.text(1.02,1,
		#	'\n'.join([
		#		'$\\bf{%s}$'%'DPTSOW22B06 \ (not \ irradiated)',
		#		# 'wafer: %s'%'22',
		#		# 'chip: %s'%'3',
		#		'version: %s'%'O',
		#		'split:  %s'%'4 (opt.)',
		#		'$I_{reset}=%s\,\\mathrm{pA}$' %10,
		#		'$I_{bias}=%s\,\\mathrm{nA}$' %100,
		#		'$I_{biasn}=%s\,\\mathrm{nA}$' %10,
		#		'$I_{db}=%s\,\\mathrm{nA}$'   %50,
		#		'$V_{casn}=%s\,\\mathrm{mV}$' %300,
		#		'$V_{casb}=%s\,\\mathrm{mV}$' %200,
		#		'$V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
		#		'$thr\\approx%s\\mathrm{e^{-}}$' %200
		#	]),
		#	fontsize=15,
		#	ha='left', va='top',
		#	transform=ax1.transAxes
		#)	
		#ax1.text(1.02,0.6,
		#	'\n'.join([
		#		'$\\bf{%s}$'%'DPTSOW22B07 \ (not \ irradiated)',
		#
		#		# 'wafer: %s'%'22',
		#		# 'chip: %s'%'3',
		#		'version: %s'%'O',
		#		'split:  %s'%'4 (opt.)',
		#		'$I_{reset}=%s\,\\mathrm{pA}$' %35,
		#		'$I_{bias}=%s\,\\mathrm{nA}$' %100,
		#		'$I_{biasn}=%s\,\\mathrm{nA}$' %10,
		#		'$I_{db}=%s\,\\mathrm{nA}$'   %50,
		#		'$V_{casn}=%s\,\\mathrm{mV}$' %300,
		#		'$V_{casb}=%s\,\\mathrm{mV}$' %250,
		#		'$V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
		#		'$thr=%s\\mathrm{e^{-}}$' %156
		#	]),
		#	fontsize=15,
		#	ha='left', va='top',
		#	transform=ax1.transAxes
		#)	
		#ax1.text(1.32,0.6,
		#	'\n'.join([
		#		'$\\bf{%s}$'%'DPTSOW22B17 \ (irradiated)',
		#		# 'wafer: %s'%'22',
		#		# 'chip: %s'%'3',
		#		'$10^{15}$ 1MeV n$_{eq}$ cm$^2$',
		#		'version: %s'%'O',
		#		'split:  %s'%'4 (opt.)',
		#		'$I_{reset}=%s\,\\mathrm{pA}$' %35,
		#		'$I_{bias}=%s\,\\mathrm{nA}$' %100,
		#		'$I_{biasn}=%s\,\\mathrm{nA}$' %10,
		#		'$I_{db}=%s\,\\mathrm{nA}$'   %50,
		#		'$V_{casn}=%s\,\\mathrm{mV}$' %300,
		#		'$V_{casb}=%s\,\\mathrm{mV}$' %250,
		#		'$V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
		#		'$thr=%s\\mathrm{e^{-}}$' %156
		#	]),
		#	fontsize=15,
		#	ha='left', va='top',
		#	transform=ax1.transAxes
		#)	

		#plt.show()
		plt.savefig(os.getcwd() + r'/ToT_plots/' + r'/ToT_histogram.jpg',dpi=fig1.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()