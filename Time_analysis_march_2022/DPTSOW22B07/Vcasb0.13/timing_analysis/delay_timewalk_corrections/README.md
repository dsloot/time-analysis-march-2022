# DPTS Testbeam Analysis - timewalk correction 

Python software to evaluate column line delay and timewalk corrections for the successive timing resolution analysis

_______________
_______________

## Usage principles

All the scripts have been designed to be used as terminal commands. Use the -h flag to access the help.
The scripts read the configuration files DPTS.yml to complete the analysis and produce the plots.

1) Modify DPTS.yml with the names of the devices to be analyzed - Example provided for DESY March 2022 DUT, Vbb = -1.2 V
2) Run gidvsToA.py to plot the ToA/Column correlations for the DUT.
3) Run GID_corrections.py to evaluate the delay corrections for column lines. Single line corrections saved into .npy files into properly created offset_correction folder.
4) Run timewalk_correction.py to evaluate the timewalk correction. Timewalk function coefficients saved into .npy files into properly created timewalk_correction folder.

_________________________
_________________________

## Example

List of commands with example flags.

	$ ./gidvsToA.py
	$ ./GID_delay_equalization.py -f ../analysis/dataframe_csv_files/filtered_dataframe.csv -p
	$ ./timewalk_correction.py -f ../analysis/dataframe_csv_files/filtered_dataframe.csv -p

