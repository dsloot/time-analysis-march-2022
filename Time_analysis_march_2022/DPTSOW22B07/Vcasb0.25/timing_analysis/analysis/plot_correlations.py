#!/usr/bin/env python3

import os, argparse, yaml
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from math import ceil

def arguments():
    parser=argparse.ArgumentParser(description='Plot ToT/ToA correlation')
    parser.add_argument('--filepath',          	'-f', 	 required=True,            help='.csv file for correlation/time resolution plots')
    parser.add_argument('--plot',               '-p',    action='store_true',      help='plot ToT/ToA no corrections')
    parser.add_argument('--corrected_offset',   '-o',    action='store_true',      help='plot ToT/ToA after line offset correction')
    parser.add_argument('--corrected_timewalk', '-t',    action='store_true',      help='plot ToT/ToA after timewalk correction')
    parser.add_argument('--gaussian_fit',		'-g',    action='store_true',      help='plot ToA histogram and perform a gaussian fit after timewalk correction')
    parser.add_argument('--ToT_plot',			'-tot',	 action='store_true',      help='plot ToT histogram')
    parser.add_argument('--side_hist',          '-sh',   action='store_true',      help='timewalk corrected map with 1D histograms on margins')    
    parser.add_argument('--histogram_slice',    '-hi',   action='store_true',      help='Plot projected ToT slice over ToA axis')
    parser.add_argument('--ToT_bin_width',	    '-tbw',  type=float, default=80,   help='histogram ToT bin width in ns - default: 90')
    parser.add_argument('--ToA_bin_width',	    '-abw',  type=float, default=4,    help='histogram ToA bin width in ns - default: 2')    
    args=parser.parse_args()
    return args


def read_yaml(filename):
    with open(filename, 'r') as file:
        config = yaml.safe_load(file)
    return config

# define gaussian function for fit
def gaussian(x, a, b, c):
    return a * np.exp(-np.power(x - b, 2) / (2 * np.power(c, 2)))


if __name__ == "__main__":
	args = arguments()
	dpts_dict = read_yaml(os.getcwd() + r"/DPTS.yml")	
	# load dataset from given directory
	df = pd.read_csv(args.filepath, sep='|', header=0)

	plot_dir0 = os.getcwd() + r'/correlation_plots'
	for d in [plot_dir0]:
		if not os.path.isdir(d):
			os.makedirs(d)

	# Creating bins
	bin_width_ToT = args.ToT_bin_width  # ns 
	bin_width_ToA = args.ToA_bin_width  # ns
	
	dpts_0 = df[df['detector'] == 'DPTS_0']
	dpts_0 = dpts_0.query('timestamp < 3001 & timestamp > 499')  # SQL way
	dpts_1 = df[df['detector'] == 'DPTS_1']	
	dpts_1 = dpts_1.query('timestamp < 3001 & timestamp > 499')  # SQL way	

	if args.plot:
		dpts0ToA = (dpts_0['timestamp'].values.astype('float'))  # in ns
		dpts0ToT = (dpts_0['ToT'].values.astype('float'))  # in ns
		x_min = np.min(dpts0ToT)
		x_max = np.max(dpts0ToT)
		y_min = np.min(dpts0ToA)
		y_max = np.max(dpts0ToA)
		x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
		y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA)) 
		## Compute 2d histogram
		H_dpts0, ToAedges_dpts0, ToTedges_dpts0 = np.histogram2d(dpts0ToA, dpts0ToT, bins=[y_bins, x_bins])
		## Find the bin centres
		ToTbinsctr_dpts0 = np.array([0.5 * (ToTedges_dpts0[i] + ToTedges_dpts0[i+1]) for i in range(len(ToTedges_dpts0)-1)])
		ToAbinsctr_dpts0 = np.array([0.5 * (ToAedges_dpts0[i] + ToAedges_dpts0[i+1]) for i in range(len(ToAedges_dpts0)-1)])
		## DPTS4
		dpts1ToA = (dpts_1['timestamp'].values.astype('float'))  # in ns
		dpts1ToT = (dpts_1['ToT'].values.astype('float'))  # in ns
		x_min = np.min(dpts1ToT)
		x_max = np.max(dpts1ToT)
		y_min = np.min(dpts1ToA)
		y_max = np.max(dpts1ToA)
		x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
		y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA))  
		## Compute 2d histogram
		H_dpts1, ToAedges_dpts1, ToTedges_dpts1 = np.histogram2d(dpts1ToA, dpts1ToT, bins=[y_bins, x_bins])
		# Find the bin centres
		ToTbinsctr_dpts1 = np.array([0.5 * (ToTedges_dpts1[i] + ToTedges_dpts1[i+1]) for i in range(len(ToTedges_dpts1)-1)])
		ToAbinsctr_dpts1 = np.array([0.5 * (ToAedges_dpts1[i] + ToAedges_dpts1[i+1]) for i in range(len(ToAedges_dpts1)-1)])

 		## Plot histogram using pcolormesh	
		fig1, ax1 = plt.subplots(figsize =(21, 10)) 
		im1 = ax1.pcolormesh(ToTedges_dpts0, ToAedges_dpts0, H_dpts0, cmap=plt.cm.viridis)
		ax1.set_xlim(dpts0ToT.min(), dpts0ToT.max())
		# ax1.set_xlim(dpts0ToT.min(), 1150)
		#ax1.set_ylim(dpts0ToA.min(), dpts0ToA.max())
		#ax1.set_ylim(990, 1010)	
		ax1.set_xlabel('ToT [ns]', fontsize=25)  # , fontweight='bold')
		ax1.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax1.set_title(f'ToA/ToT distribution - {dpts_dict["DPTS_0"]} - vcasb0.25', fontsize=25)
		cb1 = plt.colorbar(im1, ax=ax1)
		cb1.ax.tick_params(labelsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax1.grid()
		plt.tight_layout()
		# plt.show()
		fig1.savefig(os.getcwd() + r'/correlation_plots' + r'/{}_ToTvsToA_correlation_vcasb0.25.jpg'.format(dpts_dict["DPTS_0"]), dpi=fig1.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()
 		## Plot histogram using pcolormesh	
		fig2, ax2 = plt.subplots(figsize =(21, 10)) 
		im2 = ax2.pcolormesh(ToTedges_dpts1, ToAedges_dpts1, H_dpts1, cmap=plt.cm.viridis)
		ax2.set_xlim(dpts1ToT.min(), dpts1ToT.max())
		# ax2.set_xlim(dpts1ToT.min(), 1150)	
		#ax2.set_ylim(dpts1ToA.min(), dpts1ToA.max())
		ax2.set_ylim(950, 1100)	
		# ax2.set_ylim(750, 1500)	
		ax2.set_xlabel('ToT [ns]', fontsize=25)  #, fontweight='bold')
		ax2.set_ylabel('ToA [ns]', fontsize=25)  #, fontweight='bold')
		ax2.set_title(f'ToA/ToT distribution - {dpts_dict["DPTS_1"]} - vcasb0.25', fontsize=25)
		cb2 = plt.colorbar(im2, ax=ax2)
		cb2.ax.tick_params(labelsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)	
		ax2.grid()
		plt.tight_layout()
		# plt.show()
		fig2.savefig(os.getcwd() + r'/correlation_plots' + r'/{}_ToTvsToA_correlation_vcasb0.25.jpg'.format(dpts_dict["DPTS_1"]), dpi=fig2.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()

	if args.corrected_offset:
		dpts0ToA = (dpts_0['ToA_removed_offset'].values.astype('float'))  # in ns
		dpts0ToT = (dpts_0['ToT'].values.astype('float'))  # in ns
		x_min = np.min(dpts0ToT)
		x_max = np.max(dpts0ToT)
		y_min = np.min(dpts0ToA)
		y_max = np.max(dpts0ToA)
		x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
		y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA)) 
		## Compute 2d histogram
		H_dpts0, ToAedges_dpts0, ToTedges_dpts0 = np.histogram2d(dpts0ToA, dpts0ToT, bins=[y_bins, x_bins])
		## Find the bin centres
		ToTbinsctr_dpts0 = np.array([0.5 * (ToTedges_dpts0[i] + ToTedges_dpts0[i+1]) for i in range(len(ToTedges_dpts0)-1)])
		ToAbinsctr_dpts0 = np.array([0.5 * (ToAedges_dpts0[i] + ToAedges_dpts0[i+1]) for i in range(len(ToAedges_dpts0)-1)])

 		## Plot histogram using pcolormesh	
		fig1, ax1 = plt.subplots(figsize =(21, 10)) 
		im1 = ax1.pcolormesh(ToTedges_dpts0, ToAedges_dpts0, H_dpts0, cmap=plt.cm.viridis)
		ax1.set_xlim(dpts0ToT.min(), dpts0ToT.max())
		# ax1.set_xlim(dpts0ToT.min(), 1150)
		ax1.set_ylim(dpts0ToA.min(), dpts0ToA.max())
		# ax1.set_ylim(750, 1500)	
		ax1.set_xlabel('ToT [ns]', fontsize=25)  # , fontweight='bold')
		ax1.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax1.set_title(f'ToA/ToT distribution - corrected offset - {dpts_dict["DPTS_0"]} - vcasb0.25', fontsize=25)
		cb1 = plt.colorbar(im1, ax=ax1)
		cb1.ax.tick_params(labelsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax1.grid()
		plt.tight_layout()
		# plt.show()
		fig1.savefig(os.getcwd() + r'/correlation_plots' + r'/{}_ToTvsToA_correlation-corrected_offset_vcasb0.25.jpg'.format(dpts_dict["DPTS_0"]), dpi=fig1.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()

	if args.gaussian_fit:
		# Create directory if it doesn't exist yet
		gauss_plot = os.getcwd() + r'/time_distribution_plots'
		for d in [gauss_plot]:
			if not os.path.isdir(d):
				os.makedirs(d)

		dpts0ToA = (dpts_0['ToA_timewalk_corrected'].values.astype('float'))  # in ns
		x_min = np.min(dpts0ToA)
		x_max = np.max(dpts0ToA)
		x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToA))
		# Create data values for fitting
		data_entries, bins = np.histogram(dpts0ToA, bins = x_bins)
		xspace = []
		yspace = []
		for i in range(len(x_bins)-1):
			if (bins[i] > -45 and bins[i] < 45):
				xspace = np.append(xspace, bins[i])
				yspace = np.append(yspace, data_entries[i])
				x_extra = bins[i]
		xspace = np.append(xspace, x_extra)
		fit_binscenters = np.array([0.5 * (xspace[i] + xspace[i+1]) for i in range(len(xspace)-1)])
		binscenters = np.array([0.5 * (bins[i] + bins[i+1]) for i in range(len(x_bins)-1)])
		events = sum(data_entries)
		
		rms_xspace = []
		rms_yspace = []
		for i in range(len(x_bins)-1):
			if (bins[i] > -42 and bins[i] < 42):
				rms_xspace = np.append(rms_xspace, bins[i])
				rms_yspace = np.append(rms_yspace, data_entries[i])
				rms_x_extra = bins[i]
		rms_xspace = np.append(rms_xspace, rms_x_extra)
		data = sum(rms_yspace)
		percent = data/events * 100
		X_RMS = np.sqrt(np.mean(rms_xspace**2))
		print(percent)
		print(X_RMS)

		# Perform gaussian fit over the data and return parameter values
		popt, pcov = curve_fit(gaussian, xdata=fit_binscenters, ydata=yspace, p0=[300, 0, 100])
		# Combine x and y deviation into total standard deviation 
		perr = np.sqrt(np.diag(pcov))

		## Plot histogram and gaussian fit 
		fig1, ax1 = plt.subplots(figsize=(15,10))
		im1 = ax1.bar(binscenters, data_entries, width= x_bins[1]-x_bins[0], color='navy', label=r'Histogram entries')
		xspace = np.linspace(x_min, x_max, 100000)
		im2 = ax1.plot(xspace, gaussian(xspace, *popt), color='darkorange', linewidth=2.5, label=r'Gaussian fit')

 		# Tune the plot
		ax1.set_xlim(-150, 150)
		ax1.set_xlabel('ToA (ns)', fontsize=24, fontweight='bold')
		ax1.set_ylabel('Events', fontsize=24, fontweight='bold')
		ax1.set_title(f'ToA distribution - {dpts_dict["DPTS_0"]} - vcasb0.25', fontsize=24, fontweight='bold')
		plt.xticks(fontsize=20)
		plt.yticks(fontsize=20)
		plt.grid()
		plt.tight_layout()

		x = 0.18
		y = 0.98
		ax1.text(
			x-0.17,y,
			# '$\\bf{ITS3}$ beam test $\\it{%s}$',
			'Test beam preliminary $\\it{%s}$',
			fontsize=25,
			ha='left', va='top',
			# color='white',
			transform=ax1.transAxes
		)		
		#ax1.text(
		#	# x-0.01,y-0.06,
		#	x-0.17,y-0.06,
		#	'B01 & B03: @DESY Sept 2021, 5.4 GeV/c electrons',
		#	fontsize=22,
		#	ha='center', va='top',
		#	# color='white',
		#	transform=ax1.transAxes
		#)
		ax1.text(
			# x-0.01,y-0.06,
			x-0.17,y-0.06,
			'@DESY March 2022, 3.4 GeV/c electrons',
			fontsize=22,
			ha='left', va='top',
			# color='white',
			transform=ax1.transAxes
		)
		ax1.text(
			x-0.17,y-0.12,
			'Plotted on 5 July 2022',   
			fontsize=18,
			ha='left', va='top',
			# color='white',
			transform=ax1.transAxes
		)
		ax1.text(x-0.17,y-0.19,
			'\n'.join([
				'$\\bf{%s}$'%'DPTSOW22B07 \ (not \ irradiated)',
				# 'wafer: %s'%'22',
				# 'chip: %s'%'3',
				'version: %s'%'O',
				'split:  %s'%'4 (opt.)',
				'$I_{reset}=%s\,\\mathrm{pA}$' %35,
				'$I_{bias}=%s\,\\mathrm{nA}$' %100,
				'$I_{biasn}=%s\,\\mathrm{nA}$' %10,
				'$I_{db}=%s\,\\mathrm{nA}$'   %50,
				'$V_{casn}=%s\,\\mathrm{mV}$' %300,
				'$V_{casb}=%s\,\\mathrm{mV}$' %250,
				'$V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
				'$thr=%s\\mathrm{e^{-}}$' %156
			]),
			fontsize=18,
			ha='left', va='top',
			transform=ax1.transAxes
		)	

		## Print rounded parameter values + std on the plot
		textstr = '\n'.join((
			r'Events = {}'.format(events),
			r'fit Amplitude = {} $\pm$ {}'.format(round(popt[0]), round(perr[0],2)),
			r'fit $\mu$ = {} $\pm$ {}'.format(round(popt[1],3), round(perr[1],2)),
			r'fit $\sigma$ = {} $\pm$ {}'.format(round(abs(popt[2]),2), round(perr[2],2)),
			r'{}% RMS = {}'.format(round(percent), round(X_RMS, 2)),
			r'Bin width = {}'.format(bin_width_ToA)))
		# Patch properties for the textbox
		props = dict(boxstyle='round', facecolor='white', alpha=0.5)
		x_text = 0.9 * 40 #bins.min()
		y_text = 1.025 * data_entries.max()
		# Place a textbox in the upper left corner of the plot
		plt.text(x_text, y_text, textstr, fontsize=25, verticalalignment='top', bbox=props)

		#plt.show()
		plt.savefig(os.getcwd() + r'/time_distribution_plots/' + r'/{}_ToA_gaussian_fit_vcasb0.25.jpg'.format(dpts_dict["DPTS_0"]), bbox_inches='tight', pad_inches=0.5)
		plt.close()

	if args.ToT_plot:
		# Create directory if it doesn't exist yet
		ToT_plot = os.getcwd() + r'/time_distribution_plots'
		for d in [ToT_plot]:
			if not os.path.isdir(d):
				os.makedirs(d)

		dpts0ToT = (dpts_0['ToT'].values.astype('float'))  # in ns
		x_min = np.min(dpts0ToT)
		x_max = np.max(dpts0ToT)
		x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
		dpts1ToT = (dpts_1['ToT'].values.astype('float'))  # in ns
		x_min = np.min(dpts1ToT)
		x_max = np.max(dpts1ToT)
		x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
		
		plt.figure(figsize=(15,10))
		plt.hist(dpts1ToT, bins = x_bins, color='navy', alpha=0.8, label='{}'.format(dpts_dict["DPTS_1"]))
		plt.hist(dpts0ToT, bins = x_bins, color='darkorange', alpha=0.8, label='{}'.format(dpts_dict["DPTS_0"]))
		plt.xlim(0, 40000)
		plt.ylim(0, 280)
		plt.xlabel('ToT [ns]', fontsize=20)  # , fontweight='bold')
		plt.ylabel('Events', fontsize=20)  # , fontweight='bold')
		plt.title('ToT distribution - vcasb0.25', fontsize=20)
		plt.xticks(fontsize=20)
		plt.yticks(fontsize=20)
		plt.grid()
		plt.tight_layout()
		plt.legend(ncol=2, loc='upper center')

		textstr = '\n'.join((
			r'Events B7 = {}'.format(len(dpts0ToT)),
			r'Events B6 = {}'.format(len(dpts1ToT)),
			r'Bin width = {}'.format(bin_width_ToT)))
		# Patch properties for the textbox
		props = dict(boxstyle='round', facecolor='white', alpha=0.5)
		x_text = 0.85 * 40000 #bins.max()
		y_text = 0.98 * 280 #data_entries.max()
		# Place a textbox in the upper left corner of the plot
		plt.text(x_text, y_text, textstr, fontsize=14, verticalalignment='top', bbox=props)

		#plt.show()
		plt.savefig(os.getcwd() + r'/time_distribution_plots/' + r'/ToT_histogram_vcasb0.25.jpg', bbox_inches='tight', pad_inches=0.5)
		plt.close()

		dpts0ToT = (dpts_0['ToT'].values.astype('float'))  # in ns
		x_min = np.min(dpts0ToT)
		x_max = np.max(dpts0ToT)
		x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
		
		plt.figure(figsize=(15,10))
		plt.hist(dpts0ToT, bins = x_bins, color='navy')
		plt.xlim(0, 40000)
		plt.ylim(0, 280)
		plt.xlabel('ToT [ns]', fontsize=20)  # , fontweight='bold')
		plt.ylabel('Events', fontsize=20)  # , fontweight='bold')
		plt.title(f'ToT distribution - {dpts_dict["DPTS_0"]} - vcasb0.25', fontsize=20)
		plt.xticks(fontsize=20)
		plt.yticks(fontsize=20)
		plt.grid()
		plt.tight_layout()

		textstr = '\n'.join((
			r'Events = {}'.format(len(dpts0ToT)),
			r'Bin width = {}'.format(bin_width_ToT)))
		# Patch properties for the textbox
		props = dict(boxstyle='round', facecolor='white', alpha=0.5)
		x_text = 0.87 * 40000 #bins.max()
		y_text = 0.98 * 280 #data_entries.max()
		# Place a textbox in the upper left corner of the plot
		plt.text(x_text, y_text, textstr, fontsize=14, verticalalignment='top', bbox=props)

		#plt.show()
		plt.savefig(os.getcwd() + r'/time_distribution_plots/' + r'/{}_ToT_histogram_vcasb0.25.jpg'.format(dpts_dict["DPTS_0"]), bbox_inches='tight', pad_inches=0.5)
		plt.close()

		dpts1ToT = (dpts_1['ToT'].values.astype('float'))  # in ns
		x_min = np.min(dpts1ToT)
		x_max = np.max(dpts1ToT)
		x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
		
		plt.figure(figsize=(15,10))
		plt.hist(dpts1ToT, bins = x_bins, color='navy')
		plt.xlim(0, 40000)
		plt.ylim(0, 280)
		plt.xlabel('ToT [ns]', fontsize=20)  # , fontweight='bold')
		plt.ylabel('Events', fontsize=20)  # , fontweight='bold')
		plt.title(f'ToT distribution - {dpts_dict["DPTS_1"]} - vcasb0.25', fontsize=20)
		plt.xticks(fontsize=20)
		plt.yticks(fontsize=20)
		plt.grid()
		plt.tight_layout()

		textstr = '\n'.join((
			r'Events = {}'.format(len(dpts1ToT)),
			r'Bin width = {}'.format(bin_width_ToT)))
		# Patch properties for the textbox
		props = dict(boxstyle='round', facecolor='white', alpha=0.5)
		x_text = 0.87 * 40000 #bins.max()
		y_text = 0.98 * 280 #data_entries.max()
		# Place a textbox in the upper left corner of the plot
		plt.text(x_text, y_text, textstr, fontsize=14, verticalalignment='top', bbox=props)

		#plt.show()
		plt.savefig(os.getcwd() + r'/time_distribution_plots/' + r'/{}_ToT_histogram_vcasb0.25.jpg'.format(dpts_dict["DPTS_1"]), bbox_inches='tight', pad_inches=0.5)
		plt.close()

	if args.corrected_timewalk:
		dpts0ToA = (dpts_0['ToA_timewalk_corrected'].values.astype('float'))  # in ns
		dpts0ToT = (dpts_0['ToT'].values.astype('float'))  # in ns
		x_min = np.min(dpts0ToT)
		x_max = np.max(dpts0ToT)
		y_min = np.min(dpts0ToA)
		y_max = np.max(dpts0ToA)
		x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
		y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA)) 
		## Compute 2d histogram
		H_dpts0, ToAedges_dpts0, ToTedges_dpts0 = np.histogram2d(dpts0ToA, dpts0ToT, bins=[y_bins, x_bins])
		## Find the bin centres
		ToTbinsctr_dpts0 = np.array([0.5 * (ToTedges_dpts0[i] + ToTedges_dpts0[i+1]) for i in range(len(ToTedges_dpts0)-1)])
		ToAbinsctr_dpts0 = np.array([0.5 * (ToAedges_dpts0[i] + ToAedges_dpts0[i+1]) for i in range(len(ToAedges_dpts0)-1)])

 		## Plot histogram using pcolormesh	
		fig1, ax1 = plt.subplots(figsize =(21, 10)) 
		im1 = ax1.pcolormesh(ToTedges_dpts0, ToAedges_dpts0, H_dpts0, cmap=plt.cm.viridis)
		ax1.set_xlim(dpts0ToT.min(), dpts0ToT.max())
		# ax1.set_xlim(dpts0ToT.min(), 1150)
		#ax1.set_ylim(dpts0ToA.min(), dpts0ToA.max())
		ax1.set_ylim(-200, 200)	
		ax1.set_xlabel('ToT [ns]', fontsize=25)  # , fontweight='bold')
		ax1.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax1.set_title(f'ToA/ToT distribution - corrected timewalk - {dpts_dict["DPTS_0"]} - vcasb0.25', fontsize=25)
		cb1 = plt.colorbar(im1, ax=ax1)
		cb1.ax.tick_params(labelsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax1.grid()
		plt.tight_layout()
		x = 0.55
		y = 0.98
		ax1.text(
			x-0.17,y,
			# '$\\bf{ITS3}$ beam test $\\it{%s}$',
			'Test beam preliminary $\\it{%s}$',
			fontsize=25,
			ha='center', va='top',
			color='white',
			transform=ax1.transAxes
		)		
		ax1.text(
			# x-0.01,y-0.06,
			x-0.17,y-0.06,
			'@DESY March 2022, 3.4 GeV/c electrons',
			fontsize=22,
			ha='center', va='top',
			color='white',
			transform=ax1.transAxes
		)
		ax1.text(
			x-0.17,y-0.12,
			'Plotted on 5 July 2022',   
			fontsize=18,
			ha='center', va='top',
			color='white',
			transform=ax1.transAxes
		)
		ax1.text(x+0.15,y,
			'\n'.join([
				'$\\bf{%s}$'%'DPTSOW22B07 \ (not \ irradiated)',
				# 'wafer: %s'%'22',
				# 'chip: %s'%'3',
				'            version: %s'%'O',
				'            split:  %s'%'4 (opt.)',
				'            $I_{reset}=%s\,\\mathrm{pA}$' %35,
				'            $I_{bias}=%s\,\\mathrm{nA}$' %100,
				'            $I_{biasn}=%s\,\\mathrm{nA}$' %10,
				'            $I_{db}=%s\,\\mathrm{nA}$'   %50,
				'            $V_{casn}=%s\,\\mathrm{mV}$' %300,
				'            $V_{casb}=%s\,\\mathrm{mV}$' %250,
				'            $V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
				'            $thr=%s\\mathrm{e^{-}}$' %156
			]),
			fontsize=18,
			ha='left', va='top',
			color='white',
			transform=ax1.transAxes
		)	
		# plt.show()
		fig1.savefig(os.getcwd() + r'/correlation_plots' + r'/{}_ToTvsToA_correlation-corrected_timewalk_vcasb0.25.jpg'.format(dpts_dict["DPTS_0"]), dpi=fig1.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()


	## correlation plot slice histogram
	if args.histogram_slice:
		## DPTSOW22B03
		## plot single slice of ToT/ToA 2D histogram
		slice_hist_bin_width = 1  # [ns]
		dpts_0_slice = dpts_0.query('ToT > 5000 & ToT < 10000')  # SQL way
		dpts0ToA_slice = (dpts_0_slice['timestamp'].values.astype('float'))  # in ns
		dpts0ToA_tc_slice = (dpts_0_slice['ToA_removed_offset'].values.astype('float'))  # in ns
		dpts0ToA_twc_slice = (dpts_0_slice['ToA_timewalk_corrected'].values.astype('float'))  # in ns
		dpts0ToT_slice = (dpts_0_slice['ToT'].values.astype('float'))  # in ns
		## Plot histogram
		fig5, ax5 = plt.subplots(figsize =(21, 10))
		ax5.hist(dpts0ToA_slice, bins=ceil((np.max(dpts0ToA_slice) - np.min(dpts0ToA_slice)) / slice_hist_bin_width))
		# ax5.set_xlim(dpts0ToA_slice.min(), dpts0ToA_slice.max())
		ax5.set_xlim(824, 976)
		ax5.set_xlabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax5.set_ylabel('Occurrencies', fontsize=25)  # , fontweight='bold')
		ax5.set_title(f'{dpts_dict["DPTS_0"]} - vcasb0.25 - 5000<ToT<10000', fontsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax5.grid()
		plt.tight_layout()
		# plt.show()
		fig5.savefig(os.getcwd() + r'/correlation_plots' + r'/{}_ToAhist_ToTslice_5000_10000_vcasb0.25.jpg'.format(dpts_dict["DPTS_0"]), dpi=fig5.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()
		fig6, ax6 = plt.subplots(figsize =(21, 10))
		ax6.hist(dpts0ToA_tc_slice, bins=ceil((np.max(dpts0ToA_tc_slice) - np.min(dpts0ToA_tc_slice)) / slice_hist_bin_width))
		# ax6.set_xlim(dpts0ToA_tc_slice.min(), dpts0ToA_tc_slice.max())
		ax6.set_xlim(-1, 151)
		ax6.set_xlabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax6.set_ylabel('Occurrencies', fontsize=25)  # , fontweight='bold')
		ax6.set_title(f'{dpts_dict["DPTS_0"]} - vcasb0.25 - 5000<ToT<10000 - removed offset', fontsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax6.grid()
		plt.tight_layout()
		# plt.show()
		fig6.savefig(os.getcwd() + r'/correlation_plots' + r'/{}_ToAhist_ToTslice_5000_10000-removed_offset_vcasb0.25.jpg'.format(dpts_dict["DPTS_0"]), dpi=fig5.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()
		fig7, ax7 = plt.subplots(figsize =(21, 10))
		ax7.hist(dpts0ToA_twc_slice, bins=ceil((np.max(dpts0ToA_twc_slice) - np.min(dpts0ToA_twc_slice)) / slice_hist_bin_width))
		# ax7.set_xlim(dpts0ToA_twc_slice.min(), dpts0ToA_twc_slice.max())
		ax7.set_xlim(-51, 51)
		ax7.set_xlabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax7.set_ylabel('Occurrencies', fontsize=25)  # , fontweight='bold')
		ax7.set_title(f'{dpts_dict["DPTS_0"]} - vcasb0.25 - 5000<ToT<10000 - timewalk corrected', fontsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax7.grid()
		plt.tight_layout()
		# plt.show()
		fig7.savefig(os.getcwd() + r'/correlation_plots' + r'/{}_ToAhist_ToTslice_5000_10000-timewalk_corrected_vcasb0.25.jpg'.format(dpts_dict["DPTS_0"]), dpi=fig5.dpi, bbox_inches='tight', pad_inches=0.5)		
		plt.close()		
		## DPTSXW22B01
		## plot single slice of ToT/ToA 2D histogram
		slice_hist_bin_width = 1  # [ns]
		dpts_1_slice = dpts_1.query('ToT > 5000 & ToT < 10000')  # SQL way
		dpts1ToA_slice = (dpts_1_slice['timestamp'].values.astype('float'))  # in ns
		dpts1ToA_tc_slice = (dpts_1_slice['timestamp_removed_offset'].values.astype('float'))  # in ns
		dpts1ToA_twc_slice = (dpts_1_slice['timestamp_timewalk_corrected'].values.astype('float'))  # in ns		
		dpts1ToT_slice = (dpts_1_slice['ToT'].values.astype('float'))  # in ns
		## Plot histogram
		fig15, ax15 = plt.subplots(figsize =(21, 10))
		ax15.hist(dpts1ToA_slice, bins=ceil((np.max(dpts1ToA_slice) - np.min(dpts1ToA_slice)) / slice_hist_bin_width))
		# ax15.set_xlim(dpts1ToA_slice.min(), dpts1ToA_slice.max())
		ax15.set_xlim(824, 976)
		ax15.set_xlabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax15.set_ylabel('Occurrencies', fontsize=25)  # , fontweight='bold')
		ax15.set_title(f'{dpts_dict["DPTS_1"]} - vcasb0.25 - 5000<ToT<10000', fontsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax15.grid()
		plt.tight_layout()
		# plt.show()
		fig15.savefig(os.getcwd() + r'/correlation_plots' + r'/{}_ToAhist_ToTslice_5000_10000_vcasb0.25.jpg'.format(dpts_dict["DPTS_1"]), dpi=fig15.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()
		fig16, ax16 = plt.subplots(figsize =(21, 10))
		ax16.hist(dpts1ToA_tc_slice, bins=ceil((np.max(dpts1ToA_tc_slice) - np.min(dpts1ToA_tc_slice)) / slice_hist_bin_width))
		# ax16.set_xlim(dpts1ToA_tc_slice.min(), dpts1ToA_tc_slice.max())
		ax16.set_xlim(-1, 151)
		ax16.set_xlabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax16.set_ylabel('Occurrencies', fontsize=25)  # , fontweight='bold')
		ax16.set_title(f'{dpts_dict["DPTS_1"]} - vcasb0.25 - 5000<ToT<10000 - removed offset', fontsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax16.grid()
		plt.tight_layout()
		# plt.show()
		fig16.savefig(os.getcwd() + r'/correlation_plots' + r'/{}_ToAhist_ToTslice_5000_10000-removed_offset_vcasb0.25.jpg'.format(dpts_dict["DPTS_1"]), dpi=fig16.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()
		fig17, ax17 = plt.subplots(figsize =(21, 10))
		ax17.hist(dpts1ToA_twc_slice, bins=ceil((np.max(dpts1ToA_twc_slice) - np.min(dpts1ToA_twc_slice)) / slice_hist_bin_width))
		# ax17.set_xlim(dpts1ToA_twc_slice.min(), dpts1ToA_twc_slice.max())
		ax17.set_xlim(-51, 51)
		ax17.set_xlabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax17.set_ylabel('Occurrencies', fontsize=25)  # , fontweight='bold')
		ax17.set_title(f'{dpts_dict["DPTS_1"]} - vcasb0.25 - 5000<ToT<10000 - timewalk corrected', fontsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax17.grid()
		plt.tight_layout()
		# plt.show()
		fig17.savefig(os.getcwd() + r'/correlation_plots' + r'/{}_ToAhist_ToTslice_5000_10000-timewalk_corrected_vcasb0.25.jpg'.format(dpts_dict["DPTS_1"]), dpi=fig17.dpi, bbox_inches='tight', pad_inches=0.5)		
		plt.close()

	if args.side_hist:
		import plotly.express as px
		from plotly.express.colors import sequential
		#dpts0ToA = (dpts_0['timestamp_timewalk_corrected'].values.astype('float'))  # in ns
		dpts0ToA = (dpts_0['timestamp'].values.astype('float'))
		dpts0ToT = (dpts_0['ToT'].values.astype('float'))  # in ns
		#dpts1ToA = (dpts_1['timestamp_timewalk_corrected'].values.astype('float'))  # in ns
		dpts1ToA = (dpts_1['timestamp'].values.astype('float'))
		dpts1ToT = (dpts_1['ToT'].values.astype('float'))  # in ns
		fig3 = px.density_heatmap(x=dpts0ToT, y=dpts0ToA, marginal_x="histogram", marginal_y="histogram", labels={'x':"ToT [ns]", 'y':"ToA [ns]"}, range_y=[-100, 1000], nbinsx=ceil((np.max(dpts0ToT)-np.min(dpts0ToT))/bin_width_ToT), nbinsy=ceil((np.max(dpts0ToA)-np.min(dpts0ToA))/bin_width_ToA), title=f"{dpts_dict['DPTS_0']} ToT/ToA correlation - timewalk corrected", color_continuous_scale=sequential.haline)
		fig3.show()
		fig4 = px.density_heatmap(x=dpts1ToT, y=dpts1ToA, marginal_x="histogram", marginal_y="histogram", labels={'x':"ToT [ns]", 'y':"ToA [ns]"}, range_y=[-100, 1000], nbinsx=ceil((np.max(dpts1ToT)-np.min(dpts1ToT))/bin_width_ToT), nbinsy=ceil((np.max(dpts1ToA)-np.min(dpts1ToA))/bin_width_ToA), title=f"{dpts_dict['DPTS_1']} ToT/ToA correlation - timewalk corrected", color_continuous_scale=sequential.haline)
		fig4.show()		