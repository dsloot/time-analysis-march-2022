#!/usr/bin/env python3

import os, argparse, yaml
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from math import ceil
from scipy.optimize import curve_fit
from lmfit import Model
from tqdm import tqdm


def arguments():
    parser=argparse.ArgumentParser(description='ToA/ToT correlation plot')
    parser.add_argument('--filepath',             '-f', required=True,            help='.csv files with dumped waveforms time figures')
    parser.add_argument('--plot',                 '-p', action='store_true',      help='plot 2D histogram correlation plot')
    parser.add_argument('--logplot',              '-l', action='store_true',      help='plot ToT bin peaks in loglog axis')
    parser.add_argument('--semilogplot',          '-s', action='store_true',      help='plot ToT bin peaks in semilog axis')
    parser.add_argument('--ToT_bin_width',	      '-t', type=float, default=90,   help='histogram ToT bin width in ns - default: 90')
    parser.add_argument('--ToA_bin_width',	      '-a', type=float, default=2,    help='histogram ToA bin width in ns - default: 2')
    args=parser.parse_args()
    return args


def read_yaml(filename):
    with open(filename, 'r') as file:
        config = yaml.safe_load(file)
    return config


# define gaussian function for fit
def gaussian(x, a, b, c):
    return a * np.exp(-np.power(x - b, 2) / (2 * np.power(c, 2)))


# define function to fit ToT/ToA correlation
def timewalk(x, a, b, c):
	return a * np.power(x - b, -1) + c


def offset_correction(ToA):
	offset_0_even_even = np.load(file=os.getcwd() + r'/offset_correction/offset_even_even.npy')
	offset_0_even_odd = np.load(file=os.getcwd() + r'/offset_correction/offset_even_odd.npy')
	offset_0_odd_even = np.load(file=os.getcwd() + r'/offset_correction/offset_odd_even.npy')
	offset_0_odd_odd = np.load(file=os.getcwd() + r'/offset_correction/offset_odd_odd.npy')
	new_ToA = ToA.copy()
	for i in range(len(df['px_cluster_column'])):
		if df.at[i, 'detector'] == 'DPTS_0' and df.at[i, 'px_cluster_column'] % 2 == 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 == 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				# offset correction
				#print('even_even ToA:', ToA[i])
				#print('even_even offset:', offset_0_even_even[0])
				new_ToA[i] = ToA[i] - offset_0_even_even[0]
				#print('NEW even_even ToA:', ToA[i])

			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 != 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				# offset correction
				#print('even_odd ToA:', ToA[i])
				#print('even_odd offset:', offset_0_even_odd[0])
				new_ToA[i] = ToA[i] - offset_0_even_odd[0]
				#print('NEW even_odd ToA:', ToA[i])

		if df.at[i, 'detector'] == 'DPTS_0' and df.at[i, 'px_cluster_column'] % 2 != 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 == 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				# offset correction
				#print('odd_even ToA:', ToA[i])
				#print('odd_even offset:', offset_0_odd_even[0])
				new_ToA[i] = ToA[i] - offset_0_odd_even[0]
				#print('NEW odd_even ToA:', ToA[i])
				

			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 != 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				# offset correction
				#print('odd_odd ToA:', ToA[i])
				#print('odd_odd offset:', offset_0_odd_odd[0])
				new_ToA[i] = ToA[i] - offset_0_odd_odd[0]
				#print('NEW odd_odd ToA:', ToA[i])			
	return new_ToA

if __name__ == "__main__":
    # parse the arguments passed to the script
	args = arguments()

	# load dataset
	dpts_dict = read_yaml(os.getcwd() + r"/DPTS.yml")
	df = pd.read_csv(args.filepath, sep='|', header=0)

	bin_width_ToT = args.ToT_bin_width
	bin_width_ToA = args.ToA_bin_width	

	plot_dir0 = os.getcwd() + r'/timewalk_correction_plots'
	timewalk_dir0 = os.getcwd() + r'/timewalk_correction'
	csv_dir0 = os.getcwd() + r'/dataframe_csv_files'
	for d in [plot_dir0, timewalk_dir0, csv_dir0]:
		if not os.path.isdir(d):
			os.makedirs(d)

	# DPTS_0
	new_ToA = offset_correction(df['timestamp'])
	df['ToA_removed_offset'] = new_ToA.tolist()
	df.to_csv(os.getcwd() + r'/dataframe_csv_files/updated_dataframe.csv', sep='|')

	dpts_0 = df[df['detector'] == 'DPTS_0']
	# filter data based on ToA
	dpts_0 = dpts_0.query('timestamp < 3001 & timestamp > 499')  # SQL way

	# offset correction
	dpts0ToA = (dpts_0['ToA_removed_offset'].values.astype('float'))  # in ns
	dpts0ToT = (dpts_0['ToT'].values.astype('float'))  # in ns

	# Creating bins
	x_min = np.min(dpts0ToT)
	x_max = np.max(dpts0ToT)
	y_min = np.min(dpts0ToA)
	y_max = np.max(dpts0ToA)
	x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
	y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA))
	
	## Compute 2d histogram
	H_dpts0, ToAedges_dpts0, ToTedges_dpts0 = np.histogram2d(dpts0ToA, dpts0ToT, bins=[y_bins, x_bins])
	# Find the bin centres
	ToTbinsctr_dpts0 = np.array([0.5 * (ToTedges_dpts0[i] + ToTedges_dpts0[i+1]) for i in range(len(ToTedges_dpts0)-1)])
	ToAbinsctr_dpts0 = np.array([0.5 * (ToAedges_dpts0[i] + ToAedges_dpts0[i+1]) for i in range(len(ToAedges_dpts0)-1)])
	# Find the ToA bin index corresponding to the histogram peak for each ToT bin, then the corresponding ToA bin centre
	binpeakpos = np.array([np.where(H_dpts0[:,i] == np.max(H_dpts0[:,i]))[-1][0] for i in range(len(ToTedges_dpts0)-1)])
	ToAbinpeak_dpts0 = np.array([ToAbinsctr_dpts0[binpeakpos[i]] for i in range(len(ToTedges_dpts0)-1)])
	# Find the ToT bin average ToA value
	# idx = np.where(ToTbinsctr_dpts0 > 700)[0]	
	# ToAbinavg_dpts0 = np.array([np.sum(ToAbinsctr_dpts0 * H_dpts0[:,i])/np.sum(H_dpts0[:,i]) for i in idx])
	ToAbinavg_dpts0 = np.array([])
	for i in range(len(ToTedges_dpts0)-1):
		if np.sum(H_dpts0[:,i]) == 0:
			bin_avg = float('nan')
		else:
			bin_avg = np.sum(ToAbinsctr_dpts0 * H_dpts0[:,i]) / np.sum(H_dpts0[:,i])
		ToAbinavg_dpts0 = np.append(ToAbinavg_dpts0, bin_avg)
	ToAbinavg_dpts0 = np.nan_to_num(ToAbinavg_dpts0, nan=0.0)

	## # Fit each ToT bin with a gaussian
	## fitres_dpts0 = {}
	## for i in range(len(ToTedges_dpts0)-1):
	## 	try:
	## 		pars, cov = curve_fit(f=gaussian, xdata=ToAbinsctr_dpts0, ydata=H_dpts0[:,i], p0=[np.max(H_dpts0[:,i]), ToAbinpeak_dpts0[i], bin_width_ToA], bounds=(-np.inf, np.inf))
	## 		fitres_dpts0[ToTbinsctr_dpts0[i]] = [pars[1], np.sqrt(cov[1,1])]  # add mean and std of the gaussian that fits the ToT bin
	## 	except:
	## 		fitres_dpts0[ToTbinsctr_dpts0[i]] = [ToAbinpeak_dpts0[1], np.inf]  # if the fit fails add the ToA corresponding to the peak of the histogram
	## ToAbinpeakfit_dpts0 = np.array([fitres_dpts0[key][0] for key in fitres_dpts0.keys()])
	## ToAbinpeakfit_std_dpts0 = np.array([fitres_dpts0[key][1] for key in fitres_dpts0.keys()])
	## ## clean dataset
	## # Remove low statistics bins (outliers) 
	## x_out_index = np.where(ToTbinsctr_dpts0 < 1000)
	## y_out_index = np.where(ToAbinpeakfit_dpts0[x_out_index] < 1000)
	## out_indexes_1 = (x_out_index[0][0]+y_out_index)[0]
	## x_out_index_2 = np.where(ToTbinsctr_dpts0 > 20000)
	## y_out_index_2 = np.where(ToAbinpeakfit_dpts0[x_out_index_2] < 800)
	## out_indexes_2 = (x_out_index_2[0][0]+y_out_index_2)[0]
	## out_indexes = np.append(out_indexes_1, out_indexes_2)	
	## # Identify bins where gaussian fit std is greater than mean to be excluded from the dataset
	## out_std_indexes = np.where(ToAbinpeakfit_std_dpts0/ToAbinpeakfit_dpts0 > 0.05)
	## out_indexes = np.append(out_indexes, out_std_indexes[0])
	## # remove outliers and points badly fitted
	## ToTbinsctr_dpts0_corr = np.delete(ToTbinsctr_dpts0, out_indexes)
	## ToAbinpeakfit_dpts0_corr = np.delete(ToAbinpeakfit_dpts0, out_indexes)
	## ToAbinpeakfit_std_dpts0_corr = np.delete(ToAbinpeakfit_std_dpts0, out_indexes)

	## ToA(ToT) fit - composite power law with non-linear least-squares minimization
	fit_idx = np.where(ToAbinavg_dpts0 != 0.0)[0]
	xfit = ToTbinsctr_dpts0[fit_idx]
	yfit = ToAbinavg_dpts0[fit_idx]	
	fit_idx = np.where((xfit > 2000) & (xfit < 10000))[0]
	xfit = xfit[fit_idx]
	yfit = yfit[fit_idx]	
	mod = Model(timewalk)
	mod.param_names
	mod.independent_vars
	params = mod.make_params(a=249828, b=378, c=-3)
	result = mod.fit(yfit, x=xfit, a=249828, b=378, c=-3)
	print(result.fit_report())	
	# fit parameters
	print(result.params)  # print out the fit parameters		
	a = result.params['a'].value
	b = result.params['b'].value
	c = result.params['c'].value
	pars_ToT_0 = np.array([a, b, c])
	## save fit parameters into .npy files
	with open(os.getcwd() + r'/timewalk_correction/ToT.npy', 'wb') as f1:
		np.save(f1, pars_ToT_0)

	
	if args.plot:
 		## Plot histogram using pcolormesh	
		fig1, ax1 = plt.subplots(figsize =(21, 10)) 
		im1 = ax1.pcolormesh(ToTedges_dpts0, ToAedges_dpts0, H_dpts0) # , cmap='rainbow')
		ax1.plot(ToTbinsctr_dpts0, ToAbinavg_dpts0, 'wo', markersize=2)  # avg ToA value of each ToT bin
		plt.axvline(2000, 0, 300, color='darkorange', linestyle='--')
		plt.axvline(10000, 0, 300, color='darkorange', linestyle='--')		
		# ax1.plot(ToTbinsctr_dpts0, ToAbinpeakfit_dpts0, 'wo', markersize=5)  # gaussian fit of the ToT bins
		# ax1.errorbar(ToTbinsctr_dpts0_corr, ToAbinpeakfit_dpts0_corr, yerr=ToAbinpeakfit_std_dpts0_corr, marker='o', markersize=4, linestyle='', linewidth=1.0, elinewidth=1.5)	 # clean dataset used for the fit	
		ax1.plot(ToTbinsctr_dpts0, timewalk(ToTbinsctr_dpts0, *pars_ToT_0), 'r-')
		# ax1.plot(ToTbinsctr_dpts0_corr, result.init_fit, 'k--')  # first result of high-ToT fit with lmfit
		# ax1.plot(ToTbinsctr_dpts0_corr, result.best_fit, 'b--')  # best result of high-ToT fit with lmfit
		ax1.set_xlim(dpts0ToT.min(), dpts0ToT.max())
		# ax1.set_xlim(dpts0ToT.min(), 1150)
		# ax1.set_ylim(dpts0ToA.min(), dpts0ToA.max())
		# ax1.set_ylim(750, 1500)	
		ax1.set_ylim(dpts0ToA.min(), dpts0ToA.max())
		# ax1.set_ylim(dpts0ToA.min(), 1000)
		ax1.set_xlabel('ToT [ns]', fontsize=25)  # , fontweight='bold')
		ax1.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax1.set_title('ToT/ToA correlation after column delay correction - {} - vcasb0.25'.format(dpts_dict['DPTS_0']), fontsize=25)
		cb1 = plt.colorbar(im1, ax=ax1)
		cb1.ax.tick_params(labelsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax1.grid()
		plt.tight_layout()
		x = 0.55
		y = 0.98
		ax1.text(
			x-0.17,y-0.01,
			# '$\\bf{ITS3}$ beam test $\\it{%s}$',
			'Test beam preliminary $\\it{%s}$',
			fontsize=25,
			ha='center', va='top',
			color='white',
			transform=ax1.transAxes
		)		
		ax1.text(
			# x-0.01,y-0.06,
			x-0.17,y-0.06,
			'@DESY March 2022, 3.4 GeV/c electrons',
			fontsize=22,
			ha='center', va='top',
			color='white',
			transform=ax1.transAxes
		)
		ax1.text(
			x-0.17,y-0.12,
			'Plotted on 5 July 2022',   
			fontsize=18,
			ha='center', va='top',
			color='white',
			transform=ax1.transAxes
		)
		ax1.text(x+0.14,y-0.02,
			'\n'.join([
				'$\\bf{%s}$'%'DPTSOW22B07 \ (not \ irradiated)',
				# 'wafer: %s'%'22',
				# 'chip: %s'%'3',
				'            version: %s'%'O',
				'            split:  %s'%'4 (opt.)',
				'            $I_{reset}=%s\,\\mathrm{pA}$' %35,
				'            $I_{bias}=%s\,\\mathrm{nA}$' %100,
				'            $I_{biasn}=%s\,\\mathrm{nA}$' %10,
				'            $I_{db}=%s\,\\mathrm{nA}$'   %50,
				'            $V_{casn}=%s\,\\mathrm{mV}$' %300,
				'            $V_{casb}=%s\,\\mathrm{mV}$' %250,
				'            $V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
				'            $thr=%s\\mathrm{e^{-}}$' %156
			]),
			fontsize=18,
			ha='left', va='top',
			color='white',
			transform=ax1.transAxes
		)	
		plt.show()
		fig1.savefig(os.getcwd() + r'/timewalk_correction_plots' + r'/{}_timewalk_vcasb0.25.jpg'.format(dpts_dict['DPTS_0']), dpi=fig1.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()

		## Plot histogram using pcolormesh	
		#fig2, ax2 = plt.subplots(figsize =(21, 10)) 
		#im2 = ax2.pcolormesh(ToTedges_dpts1, ToAedges_dpts1, H_dpts1) # , cmap='rainbow')
		#ax2.plot(ToTbinsctr_dpts1, ToAbinpeak_dpts1, 'ko', markersize=2)  # peak value of each ToT bin
		#ax2.plot(ToTbinsctr_dpts1, ToAbinavg_dpts1, 'wo', markersize=2)  # avg ToA value of each ToT bin		
		## ax2.plot(ToTbinsctr_dpts1, ToAbinpeakfit_dpts1, 'wo', markersize=5)  # gaussian fit of the ToT bins
		## ax2.errorbar(ToTbinsctr_dpts1_corr, ToAbinpeakfit_dpts1_corr, yerr=ToAbinpeakfit_std_dpts1_corr, marker='o', markersize=4, linestyle='', linewidth=1.0, elinewidth=1.5)	 # clean dataset used for the fit	
		#ax2.plot(ToTbinsctr_dpts1, timewalk(ToTbinsctr_dpts1, *pars_ToT_0), 'r-')
		## ax2.plot(ToTbinsctr_dpts1_corr, result.init_fit, 'k--')  # first result of high-ToT fit with lmfit
		## ax2.plot(ToTbinsctr_dpts1_corr, result.best_fit, 'b--')  # best result of high-ToT fit with lmfit
		#ax2.set_xlim(dpts1ToT.min(), dpts1ToT.max())
		## ax2.set_xlim(dpts1ToT.min(), 1150)
		## ax2.set_ylim(dpts1ToA.min(), dpts1ToA.max())
		## ax2.set_ylim(750, 1500)	
		## ax2.set_ylim(dpts1ToA.min(), dpts1ToA.max())
		#ax2.set_ylim(dpts1ToA.min(), 1000)
		#ax2.set_xlabel('ToT [ns]', fontsize=25)  # , fontweight='bold')
		#ax2.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		#ax2.set_title('{} ToT/ToA correlation after gid delay correction'.format(dpts_dict['DPTS_1']), fontsize=25)
		#cb1 = plt.colorbar(im2, ax=ax2)
		#cb1.ax.tick_params(labelsize=25)
		#plt.xticks(fontsize=25)
		#plt.yticks(fontsize=25)
		#ax2.grid()
		#plt.tight_layout()
		#plt.show()
		#fig2.savefig(os.getcwd() + r'/timewalk_correction_plots/{}'.format(dpts_dict['DPTS_1']) + r'/{}_timewalk.jpg'.format(dpts_dict['DPTS_1']), dpi=fig2.dpi, bbox_inches='tight', pad_inches=0.5)
		#plt.close()

	if args.logplot:	
		#### plot ToT bin peak points in log-log scale
		fig3, ax3 = plt.subplots(figsize =(21, 10))
		# ax3.loglog(ToTbinsctr_dpts0, ToAbinpeak_dpts0, 'ko')
		ax3.loglog(ToTbinsctr_dpts0, ToAbinavg_dpts0, 'ko', markersize=5)
		# ax3.loglog(ToTbinsctr_dpts0, ToAbinpeakfit_dpts0, 'wo', markersize=5)
		# ax3.loglog(ToTbinsctr_dpts0_corr, ToAbinpeakfit_dpts0_corr, 'bo', markersize=4)	 # clean dataset used for the fit
		# ax3.errorbar(ToTbinsctr_dpts0_corr, ToAbinpeakfit_dpts0_corr, yerr=ToAbinpeakfit_std_dpts0_corr, marker='o', markersize=4, linestyle='', linewidth=1.0, elinewidth=1.5)	 # clean dataset used for the fit
		ax3.set_title('{} - log'.format(dpts_dict['DPTS_0']), fontsize=25)  #, fontweight="bold")
		ax3.set_xlabel('ToT [ns]', fontsize=25)  #, fontweight='bold')
		ax3.set_ylabel('ToA [ns]', fontsize=25)  #, fontweight='bold')
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)	
		ax3.grid(which='both')
		plt.tight_layout()
		plt.show()
		#fig4, ax4 = plt.subplots(figsize =(21, 10))
		## ax4.loglog(ToTbinsctr_dpts1, ToAbinpeak_dpts1, 'ko')
		#ax4.loglog(ToTbinsctr_dpts1, ToAbinavg_dpts1, 'ko', markersize=5)
		## ax4.loglog(ToTbinsctr_dpts1, ToAbinpeakfit_dpts1, 'wo', markersize=5)
		## ax4.loglog(ToTbinsctr_dpts1_corr, ToAbinpeakfit_dpts1_corr, 'bo', markersize=4)	 # clean dataset used for the fit
		## ax4.errorbar(ToTbinsctr_dpts1_corr, ToAbinpeakfit_dpts1_corr, yerr=ToAbinpeakfit_std_dpts1_corr, marker='o', markersize=4, linestyle='', linewidth=1.0, elinewidth=1.5)	 # clean dataset used for the fit
		#ax4.set_title('{} - log'.format(dpts_dict['DPTS_1']), fontsize=25)  #, fontweight="bold")
		#ax4.set_xlabel('ToT [ns]', fontsize=25)  #, fontweight='bold')
		#ax4.set_ylabel('ToA [ns]', fontsize=25)  #, fontweight='bold')
		#plt.xticks(fontsize=25)
		#plt.yticks(fontsize=25)	
		#ax4.grid(which='both')
		#plt.tight_layout()
		#plt.show()

	if args.semilogplot:	
		#### plot ToT bin peak points in log-log scale
		fig5, ax5 = plt.subplots(figsize =(21, 10))
		# ax5.semilogy(ToTbinsctr_dpts0, ToAbinpeak_dpts0, 'ko')
		ax5.semilogy(ToTbinsctr_dpts0, ToAbinavg_dpts0, 'ko', markersize=5)
		# ax5.semilogy(ToTbinsctr_dpts0, ToAbinpeakfit_dpts0, 'wo', markersize=5)
		# ax5.semilogy(ToTbinsctr_dpts0_corr, ToAbinpeakfit_dpts0_corr, 'bo', markersize=4)	 # clean dataset used for the fit
		# ax5.errorbar(ToTbinsctr_dpts0_corr, ToAbinpeakfit_dpts0_corr, yerr=ToAbinpeakfit_std_dpts0_corr, marker='o', markersize=4, linestyle='', linewidth=1.0, elinewidth=1.5)	 # clean dataset used for the fit
		ax5.set_title('{} - semilog'.format(dpts_dict['DPTS_0']), fontsize=25)  #, fontweight="bold")
		ax5.set_xlabel('ToT [ns]', fontsize=25)  #, fontweight='bold')
		ax5.set_ylabel('ToA [ns]', fontsize=25)  #, fontweight='bold')
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)	
		ax5.grid(which='both')
		plt.tight_layout()
		plt.show()
		#fig6, ax6 = plt.subplots(figsize =(21, 10))
		## ax6.semilogy(ToTbinsctr_dpts1, ToAbinpeak_dpts1, 'ko')
		#ax6.semilogy(ToTbinsctr_dpts1, ToAbinavg_dpts1, 'ko', markersize=5)
		## ax6.semilogy(ToTbinsctr_dpts1, ToAbinpeakfit_dpts1, 'wo', markersize=5)
		## ax6.semilogy(ToTbinsctr_dpts1_corr, ToAbinpeakfit_dpts1_corr, 'bo', markersize=4)	 # clean dataset used for the fit
		## ax6.errorbar(ToTbinsctr_dpts1_corr, ToAbinpeakfit_dpts1_corr, yerr=ToAbinpeakfit_std_dpts1_corr, marker='o', markersize=4, linestyle='', linewidth=1.0, elinewidth=1.5)	 # clean dataset used for the fit
		#ax6.set_title('{} - semilog'.format(dpts_dict['DPTS_1']), fontsize=25)  #, fontweight="bold")
		#ax6.set_xlabel('ToT [ns]', fontsize=25)  #, fontweight='bold')
		#ax6.set_ylabel('ToA [ns]', fontsize=25)  #, fontweight='bold')
		#plt.xticks(fontsize=25)
		#plt.yticks(fontsize=25)	
		#ax6.grid(which='both')
		#plt.tight_layout()
		#plt.show()		