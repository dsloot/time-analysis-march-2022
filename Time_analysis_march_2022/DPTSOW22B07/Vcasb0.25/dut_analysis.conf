[Corryvreckan]
detectors_file   = "../geometries/aligned_telescope_2.conf"

output_directory = "../output"
histogram_file   = ""

[Metronome]
triggers=1

[EventLoaderEUDAQ2]
eudaq_loglevel = WARN
file_name = ""

[Clustering4D]
time_cut_abs     = 10s
charge_weighting = true  # if true, calculate a charge-weighted mean for the cluster centre. If false, calculate the simple arithmetic mean. Default to true
reject_by_roi    = false

[Correlations]

[Tracking4D]
track_model          = "gbl"
time_cut_abs         = 1e99
spatial_cut_abs      = 1mm,1mm
min_hits_on_track    = 6
exclude_dut          = true
require_detectors    = "ALPIDE_0", "ALPIDE_1", "ALPIDE_2", "ALPIDE_3", "ALPIDE_4", "ALPIDE_5"
momentum             = 3.4GeV
unique_cluster_usage = false  # only use a cluster for one track. In case of multiple assignment, the track with the best chi2/ndof is kept. Default to falsed
reject_by_roi        = false
max_plot_chi2        = 100    # default to 50

[FilterEvents]
max_tracks = 1

[DUTAssociation]
time_cut_abs       = 1e99
spatial_cut_abs    = 50um,50um
use_cluster_centre = true

[AnalysisDUT]
file_name = ""
time_cut_frameedge     = 1000s
spatial_cut_sensoredge = 1     # parameter to discard telescope tracks at the sensor edges in fractions of pixel pitch. Default 0.5
chi2ndof_cut           = 5     # defauly value for 3
n_time_bins            = 1000
time_binning           = 0.1ns # bin width in the time residual and correlation histograms. Default to 0.1ns.
correlations           = true  # creates correlation plots between all tracks and clusters on the DUT. Defaults to false.

[AnalysisEfficiency]
time_cut_frameedge       = 1000s
chi2ndof_cut             = 5             # default to 3
n_time_bins              = 1000          # number of bins in the time residual and correlation histograms. Default to 20000
inpixel_bin_size         = 1.0um         # bin size of the in-pixel 2D efficiency histogram. Default to 1.0um
inpixel_cut_edge         = 0.5um, 0.5um  # exclude tracks going within a cut-distance to the pixel edge. Effectively defines an in-pixel ROI. Default to 5um.
spatial_cut_sensoredge   = 1