# DPTS Testbeam Analysis - timewalk correction 

Python software to evaluate GID line delay and timewalk corrections for the successive timing resolution analysis

**Author** : Roberto Russo

**email** : r.russo@cern.ch

**Maintainer** : Roberto Russo

**Status**: Development test 

_______________
_______________

## Usage principles

All the scripts have been designed to be used as terminal commands. Use the -h flag to access the help.
The scripts read the configuration files DPTS.yml and GID_intervals.yml to complete the analysis and produce the plots.

1) Modify DPTS.yml with the names of the devices to be analyzed and GID_intervals.yml with the intervals of the 32 GID columns coming from the decoding analysis - Example provided for DESY September 2021 DUTs, Vbb = -0.6 V
2) Run gid_pidvsToA.py to plot the GID/ToA and PID/ToA correlations for the 2 DUTs.
3) Run GID_delay_equalization.py to evaluate the delay corrections for GID lines. Single line corrections saved into .npy files into properly created timewalk_correction folder.
4) Run timewalk_correction.py to evaluate the timewalk correction. Timewalk function coefficients saved into .npy files into properly created timewalk_correction folder.

_________________________
_________________________

## Example

List of commands with example flags.

	$ ./gid_pidvsToA.py
	$ ./GID_delay_equalization.py -f DPTS_figures.csv -p
	$ ./timewalk_correction.py -f DPTS_figures.csv -p

