#!/usr/bin/env python3

import os, argparse, yaml
import numpy as np
import pandas as pd
from tqdm import tqdm


def arguments():
    parser=argparse.ArgumentParser(description='Routine for data preparation of DPTS testbeam timing analysis')
    parser.add_argument('--file_path',                  '-f', required=True,       help='path of the .csv output file produced by Corryvreckan')
    parser.add_argument('--updated_df',			        '-u', action='store_true', help='when updated .csv file is used from timewalk_correction')
    parser.add_argument('--remove_offset',              '-o', action='store_true', help='add a column with offset corrected ToA to the filtered dataframe')    
    parser.add_argument('--timewalk_correction',        '-w', action='store_true', help='add a column with timewalk-corrected ToA to the filtered dataframe')
    parser.add_argument('--save_filtered_df',           '-s', action='store_true', help='save the filtered dataframe produced by removing tracks associated to none or to only one of the two DUTs')
    parser.add_argument('--global_time_distribution',   '-g', action='store_true', help='prepare and save the numpy with the time difference of all the associated tracks')
    parser.add_argument('--coupled_pixels_coordinates', '-p', action='store_true', help='save dataframe with the coordinates of pixels associated to the same track')
    parser.add_argument('--coupled_track_intercepts',   '-t', action='store_true', help='save dataframe with the intercepts of associated tracks in both devices')
    args=parser.parse_args()
    return args


def read_yaml(filename):
    with open(filename, 'r') as file:
        config = yaml.safe_load(file)
    return config


def hyperbole(x, a, x0, c):
	return a * np.power((x - x0), -1) + c


def offset_correction(detector, ToA, X, Y, offset0, offset1):
	if (detector in ['DPTS_0']):
		corr_ToA = ToA - offset0[int(X)]
	if (detector in ['DPTS_1']):
		if ((X+Y)%2 == 0):
			corr_ToA = ToA - offset1[int(X-(X%2))]
		else:
			corr_ToA = ToA - offset1[int(X-(X%2)+1)]	
	return corr_ToA


def timewalk_correction(detector, ToA, ToT, Tcorr_0):
	if (detector in ['DPTS_0']):
		corr_ToA = ToA - hyperbole(ToT, *Tcorr_0)
		return corr_ToA


def load_and_clean(filepath, update_df, rm_offset, timewalk_corr, save_df):
	# read .csv file and store it into a dataframe
	df = pd.read_csv(filepath, sep='|', header=0)
	# print(df.head())

	if update_df:

		if rm_offset:
			error = 'ERROR: Offset is already removed in "ToA_removed_offset" column'

		if timewalk_corr:
			## create a new column with corrected timestamp (timewalk correction)
			# load correction parameters
			# for DPTS0
			tw_corr_0 = np.load(file=os.getcwd() + r'/../delay_timewalk_corrections/timewalk_correction/ToT.npy', mmap_mode='r')
			# apply correction
			df['ToA_timewalk_corrected'] = df.apply(lambda row: timewalk_correction(row.detector, row.ToA_removed_offset, row.ToT, tw_corr_0), axis=1)

		if save_df:
			df.to_csv(path_or_buf=os.getcwd() + r'/dataframe_csv_files' + r"/complete_dataframe.csv", sep='|', header=True, index=False)


	else:
		# create output directories if it doesn't exist
		csv_dir0 = os.getcwd() + r'/dataframe_csv_files'
		npy_dir0 = os.getcwd() + r'/time_diff_npy_files'
		for d in [csv_dir0, npy_dir0]:
			if not os.path.isdir(d):
				os.makedirs(d)

		# change DPTS names in .csv file into DPTS_0 and DPTS_1 for later analysis
		df['detector'] = df['detector'].str.replace('DPTS_6','DPTS_0')
		df['detector'] = df['detector'].str.replace('DPTS_7','DPTS_1')

		# filter by removing not associated tracks
		is_associated = df['associated'] == 1
		df = df[is_associated]
		# print(df.head())

		# convert ToA and ToT columns in ns
		df['timestamp'] = df['timestamp'].div(1000)
		df['ToT'] = df['ToT'].div(1000)

		# apply ToT cut on trigger DPTS
		#df.drop(df.index[(df['detector'] == 'DPTS_1') & (df['ToT'] <= 7000)], inplace=True)
		#df.drop(df.index[(df['detector'] == 'DPTS_1') & (df['ToT'] >= 10000)], inplace=True)

		# print(df.groupby(['file','track']).size())

		# filter by only accepting tracks associated to both DPTS DUTs
		df = df.groupby(['file','track']).filter(lambda df: df['track'].shape[0]==2)

		if rm_offset:
			## create a new column with corrected timestamp (matrix column offset)
			# load correction parameters
			# for DPTS0
			offset_0 = np.load(file=os.getcwd() + r'/timewalk_correction/{}/offset.npy'.format(getDUTname['DPTS_0']), mmap_mode='r')
			# for DPTS1
			offset_1 = np.load(file=os.getcwd() + r'/timewalk_correction/{}/offset.npy'.format(getDUTname['DPTS_1']), mmap_mode='r')
			# apply correction
			df['timestamp_removed_offset'] = df.apply(lambda row: offset_correction(row.detector, row.timestamp, row.px_cluster_column, row.px_cluster_row, offset_0, offset_1), axis=1)

		if timewalk_corr:
			## create a new column with corrected timestamp (timewalk correction)
			# load correction parameters
			# for DPTS0
			tw_corr_0 = np.load(file=os.getcwd() + r'/timewalk_correction/{}/ToT.npy'.format(getDUTname['DPTS_0']), mmap_mode='r')
			# for DPTS1
			tw_corr_1 = np.load(file=os.getcwd() + r'/timewalk_correction/{}/ToT.npy'.format(getDUTname['DPTS_1']), mmap_mode='r')
			# apply correction
			df['timestamp_timewalk_corrected'] = df.apply(lambda row: timewalk_correction(row.detector, row.timestamp_removed_offset, row.ToT, tw_corr_0, tw_corr_1), axis=1)
		if save_df:
			df.to_csv(path_or_buf=os.getcwd() + r'/dataframe_csv_files' + r"/filtered_dataframe.csv", sep='|', header=True, index=False)
	return df


def global_time_distribution(update_df, df, rm_offset, timewalk_corr):
	if update_df:
		offset_removed_ToA = df[df['detector'] == 'DPTS_0'].loc[:]['ToA_removed_offset']
		np.save(os.getcwd() + r'/time_diff_npy_files' + r'/global_offset_removed_ToA.npy', offset_removed_ToA)
		
		if timewalk_corr:
			timewalk_corrected_ToA = df[df['detector'] == 'DPTS_0'].loc[:]['ToA_timewalk_corrected']
			np.save(os.getcwd() + r'/time_diff_npy_files' + r'/global_timewalk_corrected_ToA.npy', timewalk_corrected_ToA)
	else:	
		# find distinct run files and save into an array
		files = df['file'].drop_duplicates().to_numpy()
		# the calculated time differences are saved into time_diff and corrected_time_diff arrays 
		time_diff = np.zeros(int(df.shape[0]/2))
		offset_removed_diff = np.zeros(int(df.shape[0]/2))
		timewalk_corrected_diff = np.zeros(int(df.shape[0]/2))


		i = 0  # counter
		for file in tqdm(files):
			tracks = df[(df['file'] == file)]['track'].drop_duplicates().to_numpy()
			for track in tracks:
				# for not corrected timestamps
				time_DPTS0 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_0')].iloc[0]['timestamp']
				time_DPTS1 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_1')].iloc[0]['timestamp']	
				time_diff[i] = time_DPTS1 - time_DPTS0  # [ns]
				if rm_offset:
					# for timewalk-corrected timestamps
					offset_removed_DPTS0 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_0')].iloc[0]['ToA_removed_offset']
					offset_removed_DPTS1 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_1')].iloc[0]['ToA_removed_offset']
					offset_removed_diff[i] = offset_removed_DPTS1 - offset_removed_DPTS0  # [ns]
				if timewalk_corr:
					# for timewalk-corrected timestamps
					timewalk_corrected_DPTS0 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_0')].iloc[0]['ToA_timewalk_corrected']
					timewalk_corrected_DPTS1 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_1')].iloc[0]['ToA_timewalk_corrected']
					timewalk_corrected_diff[i] = timewalk_corrected_DPTS1 - timewalk_corrected_DPTS0  # [ns]
				i+= 1
		# print(time_diff)
		# save the time difference distribution (in ns) into a numpy array
		np.save(os.getcwd() + r'/time_diff_npy_files' + r'/global_time_diff.npy', time_diff)
		if rm_offset:
			np.save(os.getcwd() + r'/time_diff_npy_files' + r'/global_offset_removed_diff.npy', offset_removed_diff)
		if timewalk_corr:
			np.save(os.getcwd() + r'/time_diff_npy_files' + r'/global_timewalk_corrected_diff.npy', timewalk_corrected_diff)


def coupled_pixel_positions(df):
	# Coordinates based on decoded pixel positions
	# create a dataframe with eudaq run, associated track, couple of pixels hit by the given track, time difference between the two tracks
	df['px_cluster_row'] = df['px_cluster_row'].astype(int)
	df['px_cluster_column'] = df['px_cluster_column'].astype(int)	
    # initialize a dataframe where saving the data extracted from the decoded pulses
	df_couple = pd.DataFrame(
        {
            'file': pd.Series([], dtype='str'),  # run file
            'track': pd.Series([], dtype='int'),  # associated track number
            'coupled_coordinates': pd.Series([], dtype='str'),  # track intercept in DPTS_0 and DPTS_1
            'dt':  pd.Series([], dtype='float'),  # time difference between ToA in DPTS_1 and ToA in DPTS_0
            'corrected_dt':  pd.Series([], dtype='float')  # time difference between timewalk-corrected ToAs in DPTS_1 and DPTS_0            
        }
    )
	# find distinct run files and save into an array
	files = df['file'].drop_duplicates().to_numpy()
	# fill the new dataframe
	for file in tqdm(files):
		tracks = df[(df['file'] == file)]['track'].drop_duplicates().to_numpy()
		for track in tracks:
			time_DPTS0 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_0')].iloc[0]['timestamp']
			time_DPTS1 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_1')].iloc[0]['timestamp']	
			dt = time_DPTS1 - time_DPTS0  # [ns]
			corrected_time_DPTS0 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_0')].iloc[0]['timestamp_timewalk_corrected']
			corrected_time_DPTS1 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_1')].iloc[0]['timestamp_timewalk_corrected']	
			corrected_dt = corrected_time_DPTS1 - corrected_time_DPTS0  # [ns]
			x_DPTS0 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_0')].iloc[0]['px_cluster_column']
			y_DPTS0 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_0')].iloc[0]['px_cluster_row']
			x_DPTS1 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_1')].iloc[0]['px_cluster_column']
			y_DPTS1 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_1')].iloc[0]['px_cluster_row']			
			df_couple = pd.concat([df_couple,
				pd.Series({
            		'file': file,
		    		'track': track,
            		'coupled_coordinates': "({},{}),({},{})".format(x_DPTS0, y_DPTS0, x_DPTS1, y_DPTS1),
            		'dt': dt,
            		'corrected_dt': corrected_dt
				}).to_frame().T],
				ignore_index=True
			)
	df_couple.to_csv(path_or_buf=os.getcwd() + r'/dataframe_csv_files' + r"/coupled_pixels_coordinates_dataframe.csv", sep='|', header=True, index=False)


def coupled_track_intercepts(df):
	# Coordinates based on associated track intercepts
	# create a dataframe with eudaq run, associated track, couple of pixels hit by the given track, time difference between the two tracks
	df['track_intercept_row'] = df['track_intercept_row'].astype(int)
	df['track_intercept_column'] = df['track_intercept_column'].astype(int)	
    # initialize a dataframe where saving the data extracted from the decoded pulses
	df_couple = pd.DataFrame(
        {
            'file': pd.Series([], dtype='str'),  # run file
            'track': pd.Series([], dtype='int'),  # associated track number
            'coupled_coordinates': pd.Series([], dtype='str'),  # track intercept in DPTS_0 and DPTS_1
            'dt':  pd.Series([], dtype='float'),  # time difference between ToA in DPTS_1 and ToA in DPTS_0
            'corrected_dt':  pd.Series([], dtype='float')  # time difference between timewalk-corrected ToAs in DPTS_1 and DPTS_0            
        }
    )
	# find distinct run files and save into an array
	files = df['file'].drop_duplicates().to_numpy()
	# fill the new dataframe
	for file in tqdm(files):
		tracks = df[(df['file'] == file)]['track'].drop_duplicates().to_numpy()
		for track in tracks:
			time_DPTS0 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_0')].iloc[0]['timestamp']
			time_DPTS1 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_1')].iloc[0]['timestamp']	
			dt = time_DPTS1 - time_DPTS0  # [ns]
			corrected_time_DPTS0 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_0')].iloc[0]['timestamp_timewalk_corrected']
			corrected_time_DPTS1 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_1')].iloc[0]['timestamp_timewalk_corrected']	
			corrected_dt = corrected_time_DPTS1 - corrected_time_DPTS0  # [ns]			
			x_DPTS0 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_0')].iloc[0]['track_intercept_column']
			y_DPTS0 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_0')].iloc[0]['track_intercept_row']
			x_DPTS1 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_1')].iloc[0]['track_intercept_column']
			y_DPTS1 = df[(df['file'] == file) & (df['track'] == track) & (df['detector'] == 'DPTS_1')].iloc[0]['track_intercept_row']
			df_couple = pd.concat([df_couple,
				pd.Series({
            		'file': file,
		    		'track': track,
            		'coupled_coordinates': "({},{}),({},{})".format(x_DPTS0, y_DPTS0, x_DPTS1, y_DPTS1),
            		'dt': dt,
            		'corrected_dt': corrected_dt
				}).to_frame().T],
				ignore_index=True
			)					
	df_couple.to_csv(path_or_buf=os.getcwd() + r'/dataframe_csv_files' + r"/coupled_track_intercepts_dataframe.csv", sep='|', header=True, index=False)


if __name__ == "__main__":
	# parse aruments passed by the user
	args = arguments()
	getDUTname = read_yaml(os.getcwd() + r"/DPTS.yml")
	#data loading and preparation
	df = load_and_clean(args.file_path, args.updated_df, args.remove_offset, args.timewalk_correction, args.save_filtered_df)

	## PREPARE DATASET TO PLOT GLOBAL TIME DISTRIBUTION (no corrections, no pixel selection)
	if args.global_time_distribution:
		global_time_distribution(args.updated_df, df, args.remove_offset, args.timewalk_correction)

	## PREPARE DATASET WITH COORDINATES OF HIT PIXELS IN BOTH DEVICES
	# dataset based on decoded pixel positions
	if args.coupled_pixels_coordinates:
		coupled_pixel_positions(df)
	# dataset based on associated track intercepts
	if args.coupled_track_intercepts:
		coupled_track_intercepts(df)
