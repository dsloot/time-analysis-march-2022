#!/usr/bin/env python3

import os, argparse, yaml
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from math import ceil
from lmfit import Model

def arguments():
    parser=argparse.ArgumentParser(description='ToA/ToT correlation plot')
    parser.add_argument('--filepath',           '-f', 	required=True,           help='dataset')
    parser.add_argument('--plot',               '-p',   action='store_true',     help='plot ToT/ToA no corrections')
    parser.add_argument('--side_hist',          '-sh',  action='store_true',     help='timewalk corrected map with 1D histograms on margins')    
    parser.add_argument('--ToT_bin_width',	    '-t',  type=float, default=90,   help='histogram ToT bin width in ns - default: 90')
    parser.add_argument('--ToA_bin_width',	    '-a',  type=float, default=2,    help='histogram ToA bin width in ns - default: 2')    
    args=parser.parse_args()
    return args


def read_yaml(filename):
    with open(filename, 'r') as file:
        config = yaml.safe_load(file)
    return config

# define shifted hyperbole for timewalk fit
def hyperbole(x, a, x0, c):
	return a * np.power((x - x0), -1) + c


if __name__ == "__main__":
	args = arguments()
	dpts_dict = read_yaml(os.getcwd() + r"/DPTS.yml")	
	# load dataset
	df = pd.read_csv(args.filepath, sep='|', header=0)

	# Creating bins
	bin_width_ToT = args.ToT_bin_width  # ns 
	bin_width_ToA = args.ToA_bin_width  # ns

	# create output directories if it doesn't exist
	plot_dir0 = os.getcwd() + r'/offset_correction_plots'
	offset_dir0 = os.getcwd() + r'/offset_correction'
	for d in [plot_dir0, offset_dir0]:
		if not os.path.isdir(d):
			os.makedirs(d)
	
	dpts0ToA_even_even = np.array([])
	dpts0ToT_even_even= np.array([])
	dpts0ToA_even_odd= np.array([])
	dpts0ToT_even_odd= np.array([])
	dpts0ToA_odd_even= np.array([])
	dpts0ToT_odd_even= np.array([])
	dpts0ToA_odd_odd= np.array([])
	dpts0ToT_odd_odd= np.array([])
	ToAoffset0_even_even = []
	ToAoffset0_even_odd = []
	ToAoffset0_odd_even = []
	ToAoffset0_odd_odd = []

	for i in range(len(df['px_cluster_column'])):
		ToA = df.at[i, 'timestamp'].astype('float')
		ToT = df.at[i, 'ToT'].astype('float')
		if df.at[i, 'detector'] == 'DPTS_0' and df.at[i, 'px_cluster_column'] % 2 == 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 == 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				dpts0ToA_even_even = np.append(dpts0ToA_even_even, ToA)  # in ns
				dpts0ToT_even_even = np.append(dpts0ToT_even_even, ToT)  # in ns

			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 != 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				dpts0ToA_even_odd = np.append(dpts0ToA_even_odd, ToA)  # in ns
				dpts0ToT_even_odd = np.append(dpts0ToT_even_odd, ToT)  # in ns

		if df.at[i, 'detector'] == 'DPTS_0' and df.at[i, 'px_cluster_column'] % 2 != 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 == 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				dpts0ToA_odd_even = np.append(dpts0ToA_odd_even, ToA)  # in ns
				dpts0ToT_odd_even = np.append(dpts0ToT_odd_even, ToT)  # in ns

			if df.at[i+1, 'detector'] == 'DPTS_1' and df.at[i+1, 'px_cluster_column'] % 2 != 0 and df.at[i, 'timestamp'] < 3001 and df.at[i, 'timestamp'] > 499:
				dpts0ToA_odd_odd = np.append(dpts0ToA_odd_odd, ToA)  # in ns
				dpts0ToT_odd_odd = np.append(dpts0ToT_odd_odd, ToT)  # in ns

	# Both B7 and B6 have even columns
	x_min = np.min(dpts0ToT_even_even)
	x_max = np.max(dpts0ToT_even_even)
	y_min = np.min(dpts0ToA_even_even)
	y_max = np.max(dpts0ToA_even_even)
	x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
	y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA)) 
	## Compute 2d histogram
	H_dpts0_even_even, ToAedges_dpts0_even_even, ToTedges_dpts0_even_even = np.histogram2d(dpts0ToA_even_even, dpts0ToT_even_even, bins=[y_bins, x_bins])
	## Find the bin centres
	ToTbinsctr_dpts0_even_even = np.array([0.5 * (ToTedges_dpts0_even_even[i] + ToTedges_dpts0_even_even[i+1]) for i in range(len(ToTedges_dpts0_even_even)-1)])
	ToAbinsctr_dpts0_even_even = np.array([0.5 * (ToAedges_dpts0_even_even[i] + ToAedges_dpts0_even_even[i+1]) for i in range(len(ToAedges_dpts0_even_even)-1)])
	# Find the ToA bin index corresponding to the histogram peak for each ToT bin, then the corresponding ToA bin centre
	binpeakpos_even_even = np.array([np.where(H_dpts0_even_even[:,p] == np.max(H_dpts0_even_even[:,p]))[-1][0] for p in range(len(ToTedges_dpts0_even_even)-1)])
	ToAbinpeak_dpts0_even_even = np.array([ToAbinsctr_dpts0_even_even[binpeakpos_even_even[p]] for p in range(len(ToTedges_dpts0_even_even)-1)])
	# Find the ToT bin average ToA value
	ToAbinavg_dpts0_even_even = np.array([])
	for i in range(len(ToTedges_dpts0_even_even)-1):
		if np.sum(H_dpts0_even_even[:,i]) == 0:
			bin_avg = float('nan')
		else:
			bin_avg = np.sum(ToAbinsctr_dpts0_even_even * H_dpts0_even_even[:,i]) / np.sum(H_dpts0_even_even[:,i])
		ToAbinavg_dpts0_even_even = np.append(ToAbinavg_dpts0_even_even, bin_avg)

	ToAbinavg_dpts0_even_even = np.nan_to_num(ToAbinavg_dpts0_even_even, nan=0.0)
	# Clean the bin average dataset for a fit the ToT curve with hyperbolic function
	idx_fit_even_even = np.where(ToTbinsctr_dpts0_even_even > 6000)[0]
	avg_y = np.array([])
	for i in idx_fit_even_even:
		if np.sum(H_dpts0_even_even[:,i]) == 0:
			y_avg = float('nan')
		else:
			y_avg = np.sum(ToAbinsctr_dpts0_even_even * H_dpts0_even_even[:,i]) / np.sum(H_dpts0_even_even[:,i])
		avg_y = np.append(avg_y, y_avg)	

	avg_y = np.nan_to_num(avg_y, nan=0.0)
	cl_avg_y = avg_y[np.where(np.diff(avg_y) < 50)[0] + 1]
	fit_y = cl_avg_y[np.where(cl_avg_y != 0.0)[0]]
	fit_x = ToTbinsctr_dpts0_even_even[np.where(ToTbinsctr_dpts0_even_even > 6000)[0]]
	fit_x = fit_x[np.where(np.diff(avg_y) < 50)[0] + 1]
	fit_x = fit_x[np.where(cl_avg_y != 0.0)[0]]
	## ToA(ToT) fit - shifted hyperbole with non-linear least-squares minimization	
	mod_h0_even_even = Model(hyperbole)
	mod_h0_even_even.param_names
	mod_h0_even_even.independent_vars
	params_h0_even_even = mod_h0_even_even.make_params(a=263534, x0=1543, c=956)
	result_h0_even_even = mod_h0_even_even.fit(fit_y, x=fit_x, a=263534, x0=1543, c=956)		
	#print(result_h0_even_even.fit_report())	
	# fit parameters
	print(result_h0_even_even.params)  # print out the fit parameters		
	a_h0_even_even = result_h0_even_even.params['a'].value
	x0_h0_even_even = result_h0_even_even.params['x0'].value
	c_h0_even_even = result_h0_even_even.params['c'].value
	pars_ToT_0_even_even = np.array([a_h0_even_even, x0_h0_even_even, c_h0_even_even])
	# ToTcorr0.append(pars_ToT_0)
	ToAoffset0_even_even.append(c_h0_even_even)
	
	# B7 has even and B6 has odd columns
	x_min = np.min(dpts0ToT_even_odd)
	x_max = np.max(dpts0ToT_even_odd)
	y_min = np.min(dpts0ToA_even_odd)
	y_max = np.max(dpts0ToA_even_odd)
	x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
	y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA)) 
	## Compute 2d histogram
	H_dpts0_even_odd, ToAedges_dpts0_even_odd, ToTedges_dpts0_even_odd = np.histogram2d(dpts0ToA_even_odd, dpts0ToT_even_odd, bins=[y_bins, x_bins])
	## Find the bin centres
	ToTbinsctr_dpts0_even_odd = np.array([0.5 * (ToTedges_dpts0_even_odd[i] + ToTedges_dpts0_even_odd[i+1]) for i in range(len(ToTedges_dpts0_even_odd)-1)])
	ToAbinsctr_dpts0_even_odd = np.array([0.5 * (ToAedges_dpts0_even_odd[i] + ToAedges_dpts0_even_odd[i+1]) for i in range(len(ToAedges_dpts0_even_odd)-1)])
	# Find the ToA bin index corresponding to the histogram peak for each ToT bin, then the corresponding ToA bin centre
	binpeakpos_even_odd = np.array([np.where(H_dpts0_even_odd[:,p] == np.max(H_dpts0_even_odd[:,p]))[-1][0] for p in range(len(ToTedges_dpts0_even_odd)-1)])
	ToAbinpeak_dpts0_even_odd = np.array([ToAbinsctr_dpts0_even_odd[binpeakpos_even_odd[p]] for p in range(len(ToTedges_dpts0_even_odd)-1)])
	# Find the ToT bin average ToA value
	ToAbinavg_dpts0_even_odd = np.array([])
	for i in range(len(ToTedges_dpts0_even_odd)-1):
		if np.sum(H_dpts0_even_odd[:,i]) == 0:
			bin_avg = float('nan')
		else:
			bin_avg = np.sum(ToAbinsctr_dpts0_even_odd * H_dpts0_even_odd[:,i]) / np.sum(H_dpts0_even_odd[:,i])
		ToAbinavg_dpts0_even_odd = np.append(ToAbinavg_dpts0_even_odd, bin_avg)

	ToAbinavg_dpts0_even_odd = np.nan_to_num(ToAbinavg_dpts0_even_odd, nan=0.0)
	# Clean the bin average dataset for a fit the ToT curve with hyperbolic function
	idx_fit_even_odd = np.where(ToTbinsctr_dpts0_even_odd > 6000)[0]
	avg_y = np.array([])
	for i in idx_fit_even_odd:
		if np.sum(H_dpts0_even_odd[:,i]) == 0:
			y_avg = float('nan')
		else:
			y_avg = np.sum(ToAbinsctr_dpts0_even_odd * H_dpts0_even_odd[:,i]) / np.sum(H_dpts0_even_odd[:,i])
		avg_y = np.append(avg_y, y_avg)	

	avg_y = np.nan_to_num(avg_y, nan=0.0)
	cl_avg_y = avg_y[np.where(np.diff(avg_y) < 50)[0] + 1]
	fit_y = cl_avg_y[np.where(cl_avg_y != 0.0)[0]]
	fit_x = ToTbinsctr_dpts0_even_odd[np.where(ToTbinsctr_dpts0_even_odd > 6000)[0]]
	fit_x = fit_x[np.where(np.diff(avg_y) < 50)[0] + 1]
	fit_x = fit_x[np.where(cl_avg_y != 0.0)[0]]
	## ToA(ToT) fit - shifted hyperbole with non-linear least-squares minimization	
	mod_h0_even_odd = Model(hyperbole)
	mod_h0_even_odd.param_names
	mod_h0_even_odd.independent_vars
	params_h0_even_odd = mod_h0_even_odd.make_params(a=313106, x0=775, c=974)
	result_h0_even_odd = mod_h0_even_odd.fit(fit_y, x=fit_x, a=313106, x0=775, c=974)		
	#print(result_h0_even_odd.fit_report())	
	# fit parameters
	print(result_h0_even_odd.params)  # print out the fit parameters		
	a_h0_even_odd = result_h0_even_odd.params['a'].value
	x0_h0_even_odd = result_h0_even_odd.params['x0'].value
	c_h0_even_odd = result_h0_even_odd.params['c'].value
	pars_ToT_0_even_odd = np.array([a_h0_even_odd, x0_h0_even_odd, c_h0_even_odd])
	# ToTcorr0.append(pars_ToT_0)
	ToAoffset0_even_odd.append(c_h0_even_odd)

	# B7 has odd and B6 has even columns
	x_min = np.min(dpts0ToT_odd_even)
	x_max = np.max(dpts0ToT_odd_even)
	y_min = np.min(dpts0ToA_odd_even)
	y_max = np.max(dpts0ToA_odd_even)
	x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
	y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA)) 
	## Compute 2d histogram
	H_dpts0_odd_even, ToAedges_dpts0_odd_even, ToTedges_dpts0_odd_even = np.histogram2d(dpts0ToA_odd_even, dpts0ToT_odd_even, bins=[y_bins, x_bins])
	## Find the bin centres
	ToTbinsctr_dpts0_odd_even = np.array([0.5 * (ToTedges_dpts0_odd_even[i] + ToTedges_dpts0_odd_even[i+1]) for i in range(len(ToTedges_dpts0_odd_even)-1)])
	ToAbinsctr_dpts0_odd_even = np.array([0.5 * (ToAedges_dpts0_odd_even[i] + ToAedges_dpts0_odd_even[i+1]) for i in range(len(ToAedges_dpts0_odd_even)-1)])
	# Find the ToA bin index corresponding to the histogram peak for each ToT bin, then the corresponding ToA bin centre
	binpeakpos_odd_even = np.array([np.where(H_dpts0_odd_even[:,p] == np.max(H_dpts0_odd_even[:,p]))[-1][0] for p in range(len(ToTedges_dpts0_odd_even)-1)])
	ToAbinpeak_dpts0_odd_even = np.array([ToAbinsctr_dpts0_odd_even[binpeakpos_odd_even[p]] for p in range(len(ToTedges_dpts0_odd_even)-1)])
	# Find the ToT bin average ToA value
	ToAbinavg_dpts0_odd_even = np.array([])
	for i in range(len(ToTedges_dpts0_odd_even)-1):
		if np.sum(H_dpts0_odd_even[:,i]) == 0:
			bin_avg = float('nan')
		else:
			bin_avg = np.sum(ToAbinsctr_dpts0_odd_even * H_dpts0_odd_even[:,i]) / np.sum(H_dpts0_odd_even[:,i])
		ToAbinavg_dpts0_odd_even = np.append(ToAbinavg_dpts0_odd_even, bin_avg)

	ToAbinavg_dpts0_odd_even = np.nan_to_num(ToAbinavg_dpts0_odd_even, nan=0.0)
	# Clean the bin average dataset for a fit the ToT curve with hyperbolic function
	idx_fit_odd_even = np.where(ToTbinsctr_dpts0_odd_even > 6000)[0] #& (ToTbinsctr_dpts0_odd_even < 11000))[0]
	avg_y = np.array([])
	for i in idx_fit_odd_even:
		if np.sum(H_dpts0_odd_even[:,i]) == 0:
			y_avg = float('nan')
		else:
			y_avg = np.sum(ToAbinsctr_dpts0_odd_even * H_dpts0_odd_even[:,i]) / np.sum(H_dpts0_odd_even[:,i])
		avg_y = np.append(avg_y, y_avg)	

	avg_y = np.nan_to_num(avg_y, nan=0.0)
	cl_avg_y = avg_y[np.where(np.diff(avg_y) < 50)[0] + 1]
	fit_y = cl_avg_y[np.where(cl_avg_y != 0.0)[0]]
	fit_x = ToTbinsctr_dpts0_odd_even[np.where(ToTbinsctr_dpts0_odd_even > 6000)[0]] #& (ToTbinsctr_dpts0_odd_even < 11000))[0]]
	fit_x = fit_x[np.where(np.diff(avg_y) < 50)[0] + 1]
	fit_x = fit_x[np.where(cl_avg_y != 0.0)[0]]
	## ToA(ToT) fit - shifted hyperbole with non-linear least-squares minimization	
	mod_h0_odd_even = Model(hyperbole)
	mod_h0_odd_even.param_names
	mod_h0_odd_even.independent_vars
	params_h0_odd_even = mod_h0_odd_even.make_params(a=164042, x0=2085, c=940)
	result_h0_odd_even = mod_h0_odd_even.fit(fit_y, x=fit_x, a=164042, x0=2085, c=940)		
	#print(result_h0_odd_even.fit_report())	
	# fit parameters
	print(result_h0_odd_even.params)  # print out the fit parameters		
	a_h0_odd_even = result_h0_odd_even.params['a'].value
	x0_h0_odd_even = result_h0_odd_even.params['x0'].value
	c_h0_odd_even = result_h0_odd_even.params['c'].value
	pars_ToT_0_odd_even = np.array([a_h0_odd_even, x0_h0_odd_even, c_h0_odd_even])
	# ToTcorr0.append(pars_ToT_0)
	ToAoffset0_odd_even.append(c_h0_odd_even)
	
	# Both B7 and B6 have odd columns
	x_min = np.min(dpts0ToT_odd_odd)
	x_max = np.max(dpts0ToT_odd_odd)
	y_min = np.min(dpts0ToA_odd_odd)
	y_max = np.max(dpts0ToA_odd_odd)
	x_bins = np.linspace(x_min, x_max, ceil((x_max-x_min)/bin_width_ToT))
	y_bins = np.linspace(y_min, y_max, ceil((y_max-y_min)/bin_width_ToA)) 
	## Compute 2d histogram
	H_dpts0_odd_odd, ToAedges_dpts0_odd_odd, ToTedges_dpts0_odd_odd = np.histogram2d(dpts0ToA_odd_odd, dpts0ToT_odd_odd, bins=[y_bins, x_bins])
	## Find the bin centres
	ToTbinsctr_dpts0_odd_odd = np.array([0.5 * (ToTedges_dpts0_odd_odd[i] + ToTedges_dpts0_odd_odd[i+1]) for i in range(len(ToTedges_dpts0_odd_odd)-1)])
	ToAbinsctr_dpts0_odd_odd = np.array([0.5 * (ToAedges_dpts0_odd_odd[i] + ToAedges_dpts0_odd_odd[i+1]) for i in range(len(ToAedges_dpts0_odd_odd)-1)])
	# Find the ToA bin index corresponding to the histogram peak for each ToT bin, then the corresponding ToA bin centre
	binpeakpos_odd_odd = np.array([np.where(H_dpts0_odd_odd[:,p] == np.max(H_dpts0_odd_odd[:,p]))[-1][0] for p in range(len(ToTedges_dpts0_odd_odd)-1)])
	ToAbinpeak_dpts0_odd_odd = np.array([ToAbinsctr_dpts0_odd_odd[binpeakpos_odd_odd[p]] for p in range(len(ToTedges_dpts0_odd_odd)-1)])
	# Find the ToT bin average ToA value
	ToAbinavg_dpts0_odd_odd = np.array([])
	for i in range(len(ToTedges_dpts0_odd_odd)-1):
		if np.sum(H_dpts0_odd_odd[:,i]) == 0:
			bin_avg = float('nan')
		else:
			bin_avg = np.sum(ToAbinsctr_dpts0_odd_odd * H_dpts0_odd_odd[:,i]) / np.sum(H_dpts0_odd_odd[:,i])
		ToAbinavg_dpts0_odd_odd = np.append(ToAbinavg_dpts0_odd_odd, bin_avg)

	ToAbinavg_dpts0_odd_odd = np.nan_to_num(ToAbinavg_dpts0_odd_odd, nan=0.0)
	# Clean the bin average dataset for a fit the ToT curve with hyperbolic function
	idx_fit_odd_odd = np.where(ToTbinsctr_dpts0_odd_odd > 6000)[0]
	avg_y = np.array([])
	for i in idx_fit_odd_odd:
		if np.sum(H_dpts0_odd_odd[:,i]) == 0:
			y_avg = float('nan')
		else:
			y_avg = np.sum(ToAbinsctr_dpts0_odd_odd * H_dpts0_odd_odd[:,i]) / np.sum(H_dpts0_odd_odd[:,i])
		avg_y = np.append(avg_y, y_avg)	

	avg_y = np.nan_to_num(avg_y, nan=0.0)
	cl_avg_y = avg_y[np.where(np.diff(avg_y) < 40)[0] + 1]
	fit_y = cl_avg_y[np.where(cl_avg_y != 0.0)[0]]
	fit_x = ToTbinsctr_dpts0_odd_odd[np.where(ToTbinsctr_dpts0_odd_odd > 6000)[0]]
	fit_x = fit_x[np.where(np.diff(avg_y) < 40)[0] + 1]
	fit_x = fit_x[np.where(cl_avg_y != 0.0)[0]]
	## ToA(ToT) fit - shifted hyperbole with non-linear least-squares minimization	
	mod_h0_odd_odd = Model(hyperbole)
	mod_h0_odd_odd.param_names
	mod_h0_odd_odd.independent_vars
	params_h0_odd_odd = mod_h0_odd_odd.make_params(a=278809, x0=1050, c=953)
	result_h0_odd_odd = mod_h0_odd_odd.fit(fit_y, x=fit_x, a=278809, x0=1050, c=953)		
	#print(result_h0_odd_odd.fit_report())	
	# fit parameters
	print(result_h0_odd_odd.params)  # print out the fit parameters		
	a_h0_odd_odd = result_h0_odd_odd.params['a'].value
	x0_h0_odd_odd = result_h0_odd_odd.params['x0'].value
	c_h0_odd_odd = result_h0_odd_odd.params['c'].value
	pars_ToT_0_odd_odd = np.array([a_h0_odd_odd, x0_h0_odd_odd, c_h0_odd_odd])
	# ToTcorr0.append(pars_ToT_0)
	ToAoffset0_odd_odd.append(c_h0_odd_odd)

	if args.plot:
 		## Plot histogram using pcolormesh	
		fig1, ax1 = plt.subplots(figsize =(21, 10)) 
		im1 = ax1.pcolormesh(ToTedges_dpts0_even_even, ToAedges_dpts0_even_even, H_dpts0_even_even, cmap=plt.cm.viridis)
		ax1.plot(ToTbinsctr_dpts0_even_even, ToAbinavg_dpts0_even_even, 'ko', markersize=5)  # average values of the ToT bins			
		ax1.plot(ToTbinsctr_dpts0_even_even, hyperbole(ToTbinsctr_dpts0_even_even, *pars_ToT_0_even_even), 'y--')
		ax1.plot(ToTbinsctr_dpts0_even_even, c_h0_even_even*np.ones(ToTbinsctr_dpts0_even_even.size), 'r-')  # offset line
		#ax1.set_xlim(dpts0ToT_even_even.min(), dpts0ToT_even_even.max())
		ax1.set_xlim(0, 40000)
		#ax1.set_ylim(dpts0ToA_even_even.min(), dpts0ToA_even_even.max())
		ax1.set_ylim(900, 1700)	
		ax1.set_xlabel('ToT [ns]', fontsize=25)  # , fontweight='bold')
		ax1.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax1.set_title('ToA/ToT correlation - {} - vcasb0.13 - even_even'.format(dpts_dict['DPTS_0']), fontsize=25)
		cb1 = plt.colorbar(im1, ax=ax1)
		cb1.ax.tick_params(labelsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax1.grid()
		plt.tight_layout()
		x = 0.55
		y = 0.98
		ax1.text(
			x-0.17,y,
			# '$\\bf{ITS3}$ beam test $\\it{%s}$',
			'Test beam preliminary $\\it{%s}$',
			fontsize=25,
			ha='center', va='top',
			color='white',
			transform=ax1.transAxes
		)		
		ax1.text(
			# x-0.01,y-0.06,
			x-0.17,y-0.06,
			'@DESY March 2022, 3.4 GeV/c electrons',
			fontsize=22,
			ha='center', va='top',
			color='white',
			transform=ax1.transAxes
		)
		ax1.text(
			x-0.17,y-0.12,
			'Plotted on 5 July 2022',   
			fontsize=18,
			ha='center', va='top',
			color='white',
			transform=ax1.transAxes
		)
		ax1.text(x+0.15,y,
			'\n'.join([
				'$\\bf{%s}$'%'DPTSOW22B07 \ (not \ irradiated)',
				# 'wafer: %s'%'22',
				# 'chip: %s'%'3',
				'            version: %s'%'O',
				'            split:  %s'%'4 (opt.)',
				'            $I_{reset}=%s\,\\mathrm{pA}$' %35,
				'            $I_{bias}=%s\,\\mathrm{nA}$' %100,
				'            $I_{biasn}=%s\,\\mathrm{nA}$' %10,
				'            $I_{db}=%s\,\\mathrm{nA}$'   %50,
				'            $V_{casn}=%s\,\\mathrm{mV}$' %300,
				'            $V_{casb}=%s\,\\mathrm{mV}$' %130,
				'            $V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
				'            $thr=%s\\mathrm{e^{-}}$' %326
			]),
			fontsize=18,
			ha='left', va='top',
			color='white',
			transform=ax1.transAxes
		)	
		# plt.show()
		fig1.savefig(os.getcwd() + r'/offset_correction_plots' + r'/{}_line_delay_correction_even_even_vcasb0.13.jpg'.format(dpts_dict['DPTS_0']), dpi=fig1.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()
		# save offset array into a .npy file to be used for a further correction
		with open(os.getcwd() + r'/offset_correction' + r'/offset_even_even.npy', 'wb') as f1:
			np.save(f1, np.array(ToAoffset0_even_even))

 		## Plot histogram using pcolormesh	
		fig2, ax2 = plt.subplots(figsize =(21, 10)) 
		im2 = ax2.pcolormesh(ToTedges_dpts0_even_odd, ToAedges_dpts0_even_odd, H_dpts0_even_odd, cmap=plt.cm.viridis)
		ax2.plot(ToTbinsctr_dpts0_even_odd, ToAbinavg_dpts0_even_odd, 'ko', markersize=5)  # average values of the ToT bins			
		ax2.plot(ToTbinsctr_dpts0_even_odd, hyperbole(ToTbinsctr_dpts0_even_odd, *pars_ToT_0_even_odd), 'y--')
		ax2.plot(ToTbinsctr_dpts0_even_odd, c_h0_even_odd*np.ones(ToTbinsctr_dpts0_even_odd.size), 'r-')  # offset line
		#ax2.set_xlim(dpts0ToT_even_odd.min(), dpts0ToT_even_odd.max())
		ax2.set_xlim(0, 40000)
		#ax2.set_ylim(dpts0ToA_even_odd.min(), dpts0ToA_even_odd.max())
		ax2.set_ylim(900, 1700)	
		ax2.set_xlabel('ToT [ns]', fontsize=25)  # , fontweight='bold')
		ax2.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax2.set_title('ToA/ToT correlation - {} - vcasb0.13 - even_odd'.format(dpts_dict['DPTS_0']), fontsize=25)
		cb1 = plt.colorbar(im2, ax=ax2)
		cb1.ax.tick_params(labelsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax2.grid()
		plt.tight_layout()
		x = 0.5
		y = 0.94
		ax2.text(
			x-0.17,y,
			# '$\\bf{ITS3}$ beam test $\\it{%s}$',
			'Test beam preliminary $\\it{%s}$',
			fontsize=25,
			ha='center', va='top',
			color='white',
			transform=ax2.transAxes
		)		
		ax2.text(
			# x-0.01,y-0.06,
			x-0.17,y-0.06,
			'@DESY March 2022, 3.4 GeV/c electrons',
			fontsize=22,
			ha='center', va='top',
			color='white',
			transform=ax2.transAxes
		)
		ax2.text(
			x-0.17,y-0.12,
			'Plotted on 5 July 2022',   
			fontsize=18,
			ha='center', va='top',
			color='white',
			transform=ax2.transAxes
		)
		ax2.text(x+0.1,y,
			'\n'.join([
				'$\\bf{%s}$'%'DPTSOW22B07 \ (not \ irradiated)',
				# 'wafer: %s'%'22',
				# 'chip: %s'%'3',
				'            version: %s'%'O',
				'            split:  %s'%'4 (opt.)',
				'            $I_{reset}=%s\,\\mathrm{pA}$' %35,
				'            $I_{bias}=%s\,\\mathrm{nA}$' %100,
				'            $I_{biasn}=%s\,\\mathrm{nA}$' %10,
				'            $I_{db}=%s\,\\mathrm{nA}$'   %50,
				'            $V_{casn}=%s\,\\mathrm{mV}$' %300,
				'            $V_{casb}=%s\,\\mathrm{mV}$' %130,
				'            $V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
				'            $thr=%s\\mathrm{e^{-}}$' %326
			]),
			fontsize=18,
			ha='left', va='top',
			color='white',
			transform=ax2.transAxes
		)	
		# plt.show()
		fig2.savefig(os.getcwd() + r'/offset_correction_plots' + r'/{}_line_delay_correction_even_odd_vcasb0.13.jpg'.format(dpts_dict['DPTS_0']), dpi=fig2.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()
		# save offset array into a .npy file to be used for a further correction
		with open(os.getcwd() + r'/offset_correction' + r'/offset_even_odd.npy', 'wb') as f2:
			np.save(f2, np.array(ToAoffset0_even_odd))

		## Plot histogram using pcolormesh	
		fig3, ax3 = plt.subplots(figsize =(21, 10)) 
		im3 = ax3.pcolormesh(ToTedges_dpts0_odd_even, ToAedges_dpts0_odd_even, H_dpts0_odd_even, cmap=plt.cm.viridis)
		ax3.plot(ToTbinsctr_dpts0_odd_even, ToAbinavg_dpts0_odd_even, 'ko', markersize=5)  # average values of the ToT bins			
		ax3.plot(ToTbinsctr_dpts0_odd_even, hyperbole(ToTbinsctr_dpts0_odd_even, *pars_ToT_0_odd_even), 'y--')
		ax3.plot(ToTbinsctr_dpts0_odd_even, c_h0_odd_even*np.ones(ToTbinsctr_dpts0_odd_even.size), 'r-')  # offset line
		#ax3.set_xlim(dpts0ToT_odd_even.min(), dpts0ToT_odd_even.max())
		ax3.set_xlim(0, 40000)
		#ax3.set_ylim(dpts0ToA_odd_even.min(), dpts0ToA_odd_even.max())
		ax3.set_ylim(900, 1700)	
		ax3.set_xlabel('ToT [ns]', fontsize=25)  # , fontweight='bold')
		ax3.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax3.set_title('ToA/ToT correlation - {} - vcasb0.13 - odd_even'.format(dpts_dict['DPTS_0']), fontsize=25)
		cb1 = plt.colorbar(im3, ax=ax3)
		cb1.ax.tick_params(labelsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax3.grid()
		plt.tight_layout()
		x = 0.55
		y = 0.94
		ax3.text(
			x-0.17,y,
			# '$\\bf{ITS3}$ beam test $\\it{%s}$',
			'Test beam preliminary $\\it{%s}$',
			fontsize=25,
			ha='center', va='top',
			color='white',
			transform=ax3.transAxes
		)		
		ax3.text(
			# x-0.01,y-0.06,
			x-0.17,y-0.06,
			'@DESY March 2022, 3.4 GeV/c electrons',
			fontsize=22,
			ha='center', va='top',
			color='white',
			transform=ax3.transAxes
		)
		ax3.text(
			x-0.17,y-0.12,
			'Plotted on 5 July 2022',   
			fontsize=18,
			ha='center', va='top',
			color='white',
			transform=ax3.transAxes
		)
		ax3.text(x+0.15,y,
			'\n'.join([
				'$\\bf{%s}$'%'DPTSOW22B07 \ (not \ irradiated)',
				# 'wafer: %s'%'22',
				# 'chip: %s'%'3',
				'            version: %s'%'O',
				'            split:  %s'%'4 (opt.)',
				'            $I_{reset}=%s\,\\mathrm{pA}$' %35,
				'            $I_{bias}=%s\,\\mathrm{nA}$' %100,
				'            $I_{biasn}=%s\,\\mathrm{nA}$' %10,
				'            $I_{db}=%s\,\\mathrm{nA}$'   %50,
				'            $V_{casn}=%s\,\\mathrm{mV}$' %300,
				'            $V_{casb}=%s\,\\mathrm{mV}$' %130,
				'            $V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
				'            $thr=%s\\mathrm{e^{-}}$' %326
			]),
			fontsize=18,
			ha='left', va='top',
			color='white',
			transform=ax3.transAxes
		)	
		# plt.show()
		fig3.savefig(os.getcwd() + r'/offset_correction_plots' + r'/{}_line_delay_correction_odd_even_vcasb0.13.jpg'.format(dpts_dict['DPTS_0']), dpi=fig3.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()
		# save offset array into a .npy file to be used for a further correction
		with open(os.getcwd() + r'/offset_correction' + r'/offset_odd_even.npy', 'wb') as f3:
			np.save(f3, np.array(ToAoffset0_odd_even))

 		## Plot histogram using pcolormesh	
		fig4, ax4 = plt.subplots(figsize =(21, 10)) 
		im4 = ax4.pcolormesh(ToTedges_dpts0_odd_odd, ToAedges_dpts0_odd_odd, H_dpts0_odd_odd, cmap=plt.cm.viridis)
		ax4.plot(ToTbinsctr_dpts0_odd_odd, ToAbinavg_dpts0_odd_odd, 'ko', markersize=5)  # average values of the ToT bins			
		ax4.plot(ToTbinsctr_dpts0_odd_odd, hyperbole(ToTbinsctr_dpts0_odd_odd, *pars_ToT_0_odd_odd), 'y--')
		ax4.plot(ToTbinsctr_dpts0_odd_odd, c_h0_odd_odd*np.ones(ToTbinsctr_dpts0_odd_odd.size), 'r-')  # offset line
		#ax4.set_xlim(dpts0ToT_odd_odd.min(), dpts0ToT_odd_odd.max())
		ax4.set_xlim(0, 40000)
		#ax4.set_ylim(dpts0ToA_odd_odd.min(), dpts0ToA_odd_odd.max())
		ax4.set_ylim(900, 1700)	
		ax4.set_xlabel('ToT [ns]', fontsize=25)  # , fontweight='bold')
		ax4.set_ylabel('ToA [ns]', fontsize=25)  # , fontweight='bold')
		ax4.set_title('ToA/ToT correlation - {} - vcasb0.13 - odd_odd'.format(dpts_dict['DPTS_0']), fontsize=25)
		cb1 = plt.colorbar(im4, ax=ax4)
		cb1.ax.tick_params(labelsize=25)
		plt.xticks(fontsize=25)
		plt.yticks(fontsize=25)
		ax4.grid()
		plt.tight_layout()
		x = 0.55
		y = 0.98
		ax4.text(
			x-0.17,y,
			# '$\\bf{ITS3}$ beam test $\\it{%s}$',
			'Test beam preliminary $\\it{%s}$',
			fontsize=25,
			ha='center', va='top',
			color='white',
			transform=ax4.transAxes
		)		
		ax4.text(
			# x-0.01,y-0.06,
			x-0.17,y-0.06,
			'@DESY March 2022, 3.4 GeV/c electrons',
			fontsize=22,
			ha='center', va='top',
			color='white',
			transform=ax4.transAxes
		)
		ax4.text(
			x-0.17,y-0.12,
			'Plotted on 5 July 2022',   
			fontsize=18,
			ha='center', va='top',
			color='white',
			transform=ax4.transAxes
		)
		ax4.text(x+0.15,y,
			'\n'.join([
				'$\\bf{%s}$'%'DPTSOW22B07 \ (not \ irradiated)',
				# 'wafer: %s'%'22',
				# 'chip: %s'%'3',
				'            version: %s'%'O',
				'            split:  %s'%'4 (opt.)',
				'            $I_{reset}=%s\,\\mathrm{pA}$' %35,
				'            $I_{bias}=%s\,\\mathrm{nA}$' %100,
				'            $I_{biasn}=%s\,\\mathrm{nA}$' %10,
				'            $I_{db}=%s\,\\mathrm{nA}$'   %50,
				'            $V_{casn}=%s\,\\mathrm{mV}$' %300,
				'            $V_{casb}=%s\,\\mathrm{mV}$' %130,
				'            $V_{pwell}=V_{sub}=%s\\mathrm{V}$' %-1.2,
				'            $thr=%s\\mathrm{e^{-}}$' %326
			]),
			fontsize=18,
			ha='left', va='top',
			color='white',
			transform=ax4.transAxes
		)	
		# plt.show()
		fig4.savefig(os.getcwd() + r'/offset_correction_plots' + r'/{}_line_delay_correction_odd_odd_vcasb0.13.jpg'.format(dpts_dict['DPTS_0']), dpi=fig4.dpi, bbox_inches='tight', pad_inches=0.5)
		plt.close()
		# save offset array into a .npy file to be used for a further correction
		with open(os.getcwd() + r'/offset_correction' + r'/offset_odd_odd.npy', 'wb') as f4:
			np.save(f4, np.array(ToAoffset0_odd_odd))


	if args.side_hist:
		import plotly.express as px
		from plotly.express.colors import sequential
		#dpts0ToA = (dpts_0['timestamp_timewalk_corrected'].values.astype('float'))  # in ns
		dpts0ToA = (dpts_0['timestamp'].values.astype('float'))
		dpts0ToT = (dpts_0['ToT'].values.astype('float'))  # in ns
		#dpts1ToA = (dpts_1['timestamp_timewalk_corrected'].values.astype('float'))  # in ns
		dpts1ToA = (dpts_1['timestamp'].values.astype('float'))
		dpts1ToT = (dpts_1['ToT'].values.astype('float'))  # in ns
		fig3 = px.density_heatmap(x=dpts0ToT, y=dpts0ToA, marginal_x="histogram", marginal_y="histogram", labels={'x':"ToT [ns]", 'y':"ToA [ns]"}, range_y=[-100, 1000], nbinsx=ceil((np.max(dpts0ToT)-np.min(dpts0ToT))/bin_width_ToT), nbinsy=ceil((np.max(dpts0ToA)-np.min(dpts0ToA))/bin_width_ToA), title=f"{dpts_dict['DPTS_0']} ToT/ToA correlation - timewalk corrected", color_continuous_scale=sequential.haline)
		fig3.show()
		fig4 = px.density_heatmap(x=dpts1ToT, y=dpts1ToA, marginal_x="histogram", marginal_y="histogram", labels={'x':"ToT [ns]", 'y':"ToA [ns]"}, range_y=[-100, 1000], nbinsx=ceil((np.max(dpts1ToT)-np.min(dpts1ToT))/bin_width_ToT), nbinsy=ceil((np.max(dpts1ToA)-np.min(dpts1ToA))/bin_width_ToA), title=f"{dpts_dict['DPTS_1']} ToT/ToA correlation - timewalk corrected", color_continuous_scale=sequential.haline)
		fig4.show()		
